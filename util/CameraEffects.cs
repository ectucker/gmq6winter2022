﻿using Godot;

public class CameraEffects : Node
{
    public static CameraEffects Instance { get; private set; }

    private Vector2 _screenshakeOffet = Vector2.Zero;
    private Vector2 _kickbackDir = Vector2.Zero;
    private float _kickbackLength;
    
    public Vector2 Offset { get; private set; } = Vector2.Zero;
    
    public float Trauma { get; set; }

    private float _time = 0;

    private OpenSimplexNoise _noise;

    private float _hitstopTime = 0;
    
    public override void _Ready()
    {
        base._Ready();

        Instance = this;

        _noise = new OpenSimplexNoise();
        _noise.Octaves = 4;
        _noise.Period = 5.0f;
        _noise.Persistence = 0.8f;
    }

    public override void _Process(float delta)
    {
        base._Process(delta);

        _time += delta;
        Trauma = Mathf.Clamp(Trauma - delta, 0, 1);
        _kickbackLength = Mathf.Clamp(_kickbackLength - delta * 64.0f, 0, Mathf.Inf);
        
        _screenshakeOffet.x = 128.0f * Trauma * Trauma * _noise.GetNoise1d(_time);
        _screenshakeOffet.y = 128.0f * Trauma * Trauma * _noise.GetNoise1d(_time);

        Offset = _screenshakeOffet + _kickbackDir * _kickbackLength;

        _hitstopTime -= delta;
        if (_hitstopTime < 0)
            Engine.TimeScale = 1.0f;
    }

    public void Kickback(Vector2 dir, float strength)
    {
        _kickbackDir = dir;
        _kickbackLength = strength;
    }

    public void Hitstop(float duration)
    {
        Engine.TimeScale = 0.1f;
        _hitstopTime = duration * Engine.TimeScale;
    }
}