using Godot;

public class InputUtil : Node
{
    private static InputUtil _instance;

    public override void _Ready()
    {
        base._Ready();

        _instance = this;
    }

    public static Vector2 GetWorldMousePosition()
    {
        var viewport = _instance.GetViewport();
        return viewport.CanvasTransform.AffineInverse() * viewport.GetMousePosition();
    }
}
