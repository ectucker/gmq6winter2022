using Godot;
using System;
using System.Collections.Generic;

public class ParticleGroup : Node2D
{
    private List<Particles2D> _particles;
    
    public override void _Ready()
    {
        _particles = this.FindChildren<Particles2D>();
    }

    public void SetEmitting(bool emitting)
    {
        foreach (var particle in _particles)
        {
            particle.Emitting = emitting;
        }
    }
}
