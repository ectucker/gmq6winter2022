﻿using Godot;

public class RemoveAfter : Node2D
{
    private float _time = 0;

    [Export] public float Lifetime = 0.5f;
    
    public override void _Process(float delta)
    {
        base._Process(delta);
        _time += delta;
        if (_time >= Lifetime)
            QueueFree();
    }
}