using Godot;
using System;

public class Transitions : Node
{
    public static Transitions Instance { get; set; }

    private string _target;

    private AnimationPlayer _player;
    
    public override void _Ready()
    {
        base._Ready();

        Instance = this;
        _player = this.FindChild<AnimationPlayer>();
    }

    public void TransitionTo(string scene)
    {
        _target = scene;
        _player.Play("FadeTransition");
    }

    public void SwitchScene()
    {
        GetTree().ChangeScene(_target);
    }
}
