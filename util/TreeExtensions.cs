﻿using System.Collections.Generic;
using Godot;

public static class TreeExtensions
{
    public static T FindChild<T>(this Node node) where T : Node
    {
        foreach (Node child in node.GetChildren())
        {
            if (child is T t1)
                return t1;
            if (child.FindChild<T>() is T t2)
                return t2;
        }

        return default;
    }

    public static T FindParent<T>(this Node node) where T : Node
    {
        Node parent = node.GetParent();
        if (parent is T t)
            return t;
        if (parent != null)
            return parent.FindParent<T>();
        return default;
    }

    public static List<T> FindChildren<T>(this Node node) where T : Node
    {
        List<T> children = new List<T>();
        
        foreach (Node child in node.GetChildren())
        {
            if (child is T t1)
                children.Add(t1);
            var t2 = child.FindChildren<T>();
            if (t2.Count > 0)
                children.AddRange(t2);
        }

        return children;
    }
}
