﻿using Godot;

public class EmitOnReady : Particles2D
{
    public override void _Ready()
    {
        base._Ready();

        Emitting = true;
    }
}