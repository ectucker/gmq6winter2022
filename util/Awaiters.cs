﻿using System;
using System.Threading.Tasks;
using Godot;

public class Awaiters : Node
{
    private static Awaiters _instance;

    public override void _Ready()
    {
        base._Ready();

        _instance = this;
    }

    public static async Task Delay(float time, bool pauseModeProcess = true)
    {
        await _instance.ToSignal(_instance.GetTree().CreateTimer(time, pauseModeProcess), "timeout");
    }

    public static async Task<float> IdleFrame()
    {
        await _instance.ToSignal(_instance.GetTree(), "physics_frame");
        return _instance.GetProcessDeltaTime();
    }
    
    public static async Task<float> PhysicsFrame()
    {
        await _instance.ToSignal(_instance.GetTree(), "physics_frame");
        return _instance.GetPhysicsProcessDeltaTime();
    }

    public static async Task<bool> Condition(Func<bool> predicate)
    {
        try
        {
            while (!predicate())
                await PhysicsFrame();
            return true;
        }
        catch
        {
            return false;
        }
    }

    public static async Task Signal(Godot.Object source, string signal)
    {
        await _instance.ToSignal(source, signal);
    }
}