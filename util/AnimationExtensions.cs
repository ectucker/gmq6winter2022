﻿using Godot;

public static class AnimationExtensions
{
    public static void PlayIfNot(this AnimationPlayer player, string name)
    {
        if (player.CurrentAnimation != name)
            player.Play(name);
    }
}