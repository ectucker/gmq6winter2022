﻿using Godot;

public class GameState : Node
{
    private int levelIndex = 0;

    private string[] levelScenes = { "res://content/biomes/firepits/firepits.tscn", "res://content/biomes/caves/caves.tscn", "res://content/biomes/forest/forest.tscn" };
    private string endScene = "res://base/menu/end_menu/end_menu.tscn";
    
    public static GameState Instance { get; private set; }

    private Door _lastDoor;

    public bool FirstMovement = false;

    public override void _Ready()
    {
        base._Ready();

        Instance = this;
    }

    public void NextLevel(Door door)
    {
        if (door != _lastDoor)
        {
            _lastDoor = door;
            levelIndex++;
            if (levelIndex < levelScenes.Length)
                Transitions.Instance.TransitionTo(levelScenes[levelIndex]);
            else
                Transitions.Instance.TransitionTo(endScene);
        }
    }

    public void Reset()
    {
        levelIndex = 0;
        FirstMovement = false;
    }
}