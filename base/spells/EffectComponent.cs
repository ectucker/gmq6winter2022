﻿using Godot;

public abstract class EffectComponent : SpellComponent
{
    [Signal]
    public delegate void ProjectileSpawned(Projectile proj);
    
    
    public abstract void Cast(Team team, Vector2 source, Vector2 target);
}
