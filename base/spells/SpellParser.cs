﻿using System;
using System.Collections.Generic;
using System.Linq;

public static class SpellParser
{
    public static Spell ParseSpell(List<string> names)
    {
        var components = from name in names
            select SpellTokens.MakeToken(name).CreateComponent();
        return ParseSpell(components.ToList());
    }
    
    public static Spell ParseSpell(List<SpellToken> tokens)
    {
        var components = from token in tokens
            select token.CreateComponent();
        return ParseSpell(components.ToList());
    }
    
    public static Spell ParseSpell(List<SpellComponent> components)
    {
        List<Spell> spells = new List<Spell>();

        int i = 0;
        while (i < components.Count)
        {
            var parseResult = _parseSpell(components, i);
            i = parseResult.Item2;
            
            spells.Add(parseResult.Item1);
        }
        
        if (spells.Count == 0)
            spells.Add(new Spell(new SummonProjectile(), new List<ModifierComponent>()));

        return spells[0];
    }
    
    private static Tuple<Spell, int> _parseSpell(List<SpellComponent> components, int start)
    {
        List<ModifierComponent> modifiers = new List<ModifierComponent>();
        EffectComponent finalEffect = new SummonProjectile();
        int lastParsed = start;
        
        for (int i = start; i < components.Count; i++)
        {
            SpellComponent component = components[i];
            
            if (component is ModifierComponent modifier)
            {
                /*if (component is CompositeModifierComponent compModifier)
                {
                    var argResult = _parseArgs(components, i + 1, compModifier.ExpectedArguments);
                    i = argResult.Item2 - 1;
                    compModifier.Arguments = argResult.Item1;
                }*/
                
                modifiers.Add(modifier);
            }
            else if (component is EffectComponent effect)
            {
                if (component is CompositeEffectComponent compEffect)
                {
                    var argResult = _parseArgs(components, i + 1, compEffect.ExpectedArguments);
                    i = argResult.Item2 - 1;
                    compEffect.Arguments = argResult.Item1;
                }

                finalEffect = effect;
                lastParsed = i;
                break;
            }
        }

        var spell = new Spell(finalEffect, modifiers);
        return new Tuple<Spell, int>(spell, lastParsed + 1);
    }

    private static Tuple<List<Spell>, int> _parseArgs(List<SpellComponent> components, int start, int num)
    {
        int i = start;
        
        List<Spell> args = new List<Spell>();
        for (int arg = 0; arg < num; arg++)
        {
            var parseResult = _parseSpell(components, i);
            i = parseResult.Item2;
            args.Add(parseResult.Item1);
        }

        return new Tuple<List<Spell>, int>(args, i);
    }
}
