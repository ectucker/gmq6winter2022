﻿using System.Collections.Generic;
using Godot;

public class Spell : Reference
{
    public List<ModifierComponent> Modifiers { get; private set; }
    
    public EffectComponent Effect { get; private set; }

    public Spell(EffectComponent effect, List<ModifierComponent> modifiers)
    {
        Modifiers = modifiers;
        Effect = effect;
        foreach (var modifer in Modifiers)
        {
            modifer.ConnectSignals(effect);
        }
    }

    public void Cast(Team team, Vector2 source, Vector2 target)
    {
        Effect.Cast(team, source, target);
    }
}
