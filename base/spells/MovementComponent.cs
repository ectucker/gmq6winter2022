﻿using System.Threading.Tasks;
using Godot;

public abstract class MovementComponent : SpellComponent
{
    public abstract string ParticlePath { get; }

    protected Node2D _effect;

    public void DoMovement(Player player)
    {
        _effect = GlobalLocations.SpawnRoot.Spawn<Node2D>(ParticlePath);
        _effect.GlobalPosition = player.GlobalPosition;
        Apply(player);
    }
    
    public abstract Task Apply(Player player);
}