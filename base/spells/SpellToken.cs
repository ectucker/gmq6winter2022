﻿using System;
using Godot;

public class SpellToken : Reference
{
    public string Name { get; private set; }
    
    public string Description { get; private set; }
    
    public string Flavor { get; private set; }
    
    public string IconPath { get; private set; }

    private readonly Type _type;

    public SpellToken(string name, string icon, string description, string flavor, Type type)
    {
        Name = name;
        Description = description;
        Flavor = flavor;
        IconPath = icon;
        _type = type;
    }

    public SpellComponent CreateComponent()
    {
        return (SpellComponent)Activator.CreateInstance(_type);
    }
}
