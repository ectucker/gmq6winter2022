﻿using Godot;

public abstract class ModifierComponent : SpellComponent
{
    // Path to a scene representing this modifier
    // This will contain any extra functionality on the projectile itself.
    public abstract string ModiferPath { get; }

    public virtual bool HidesMain => false;

    private PackedScene _scene;
    
    public void ConnectSignals(EffectComponent effect)
    {
        _scene = GD.Load<PackedScene>(ModiferPath);
        effect.Connect(nameof(EffectComponent.ProjectileSpawned), this, nameof(ProjectileSpawnedBase));
    }

    public void ProjectileSpawnedBase(Projectile proj)
    {
        proj.Connect(nameof(Projectile.ProjectileMove), this, nameof(ModifierComponent.ProjectileMove));
        proj.Connect(nameof(Projectile.ProjectileHit), this, nameof(ModifierComponent.ProjectileHit));
        proj.Connect(nameof(Projectile.TriggerHit), this, nameof(ModifierComponent.TriggerHit));
        proj.Connect(nameof(Projectile.TimerHit), this, nameof(ModifierComponent.TimerHit));

        var particle = _scene.Instance<Node2D>();
        proj.AddChild(particle);
        EmitSignal(nameof(ProjectileSpawned), proj);

        if (HidesMain)
            proj.GetNode<Node2D>("Particle").Visible = false;
        
        ProjectileSpawned(proj);
    }
    
    /// <summary>
    /// Called whenever a projectile is spawned by a spell this modifer applies to.
    /// Properties of the projectile can be modified here
    /// </summary>
    /// <param name="proj">The spawned projectile</param>
    public virtual void ProjectileSpawned(Projectile proj)
    {
        
    }

    /// <summary>
    /// Called whenever a projectile spawned by a spell this modifer applies to is about to move.
    /// The projectiles position and velocity can be modified here for certain effects.
    /// </summary>
    /// <param name="proj">The moving projectile</param>
    /// <param name="delta">The time since the last projectile movement, in seconds</param>
    public virtual void ProjectileMove(Projectile proj, float delta)
    {
        
    }

    /// <summary>
    /// Called whenever a projectile this modifier applies to hits an enemy.
    /// Note: Any additional damage done here should involve multiplying by proj.Damage
    /// </summary>
    /// <param name="proj">The projectile</param>
    /// <param name="status">The status area for the enemy that was hit</param>
    public virtual void ProjectileHit(Projectile proj, StatusArea status)
    {
        
    }

    /// <summary>
    /// Called whenever a projectile this modifier applies to hits an enemy or a wall.
    /// </summary>
    /// <param name="proj">The projectile</param>
    public virtual void TriggerHit(Projectile proj)
    {
        
    }
    
    /// <summary>
    /// Called halfway through the lifetime of a projectile this modifier applies to.
    /// </summary>
    /// <param name="proj">The projectile</param>
    public virtual void TimerHit(Projectile proj)
    {
        
    }
}
