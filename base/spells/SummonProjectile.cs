﻿using Godot;

public class SummonProjectile : EffectComponent
{
    public string ProjectilePath => "res://content/spells/basic_missile/basic_missile.tscn";

    public override void Cast(Team team, Vector2 source, Vector2 target)
    {
        var proj = GlobalLocations.SpawnRoot.Spawn<Projectile>(ProjectilePath);
        proj.Team = team;
        proj.GlobalPosition = source;
        proj.Direction = (target - source).Normalized();
        EmitSignal(nameof(ProjectileSpawned), proj);
    }
}
