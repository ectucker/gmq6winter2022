﻿using System.Collections.Generic;

public abstract class CompositeEffectComponent : EffectComponent
{
    public abstract int ExpectedArguments { get; }

    public List<Spell> Arguments { get; set; }
}
