﻿using Godot;
using Godot.Collections;

public class Projectile : Area2D
{
    /// <summary>
    /// The damage of this projectile.
    /// Changing this should scale the damage of other modifiers using this projectile.
    /// </summary>
    [Export] public float Damage { get; set; } = 10;

    /// <summary>
    /// The base speed of this projectile.
    /// </summary>
    [Export] public float BaseSpeed { get; set; } = 1200.0f;
    
    /// <summary>
    /// The current speed of this projectile.
    /// Each frame, the projectile will move by Speed * Direction * delta
    /// </summary>
    public float Speed { get; set; }

    /// <summary>
    /// The lifetime of this projectile, in seconds.
    /// </summary>
    [Export] public float Lifetime { get; set; } = 1.5f;

    /// <summary>
    /// A dictionary used to store custom data for weirder modifiers.
    /// Any keys used in this must be unique among modifiers!
    /// </summary>
    public Dictionary<string, object> CustomData { get; }
    
    /// <summary>
    /// The direction this projectile is traveling.
    /// This is automatically set to be towards the cursor when the projectile is summoned,
    /// but it can be changed from there.
    /// </summary>
    public Vector2 Direction { get; set; } = Vector2.Right;

    /// <summary>
    /// How long the projectile has existed in the world.
    /// </summary>
    public float Time => _life;
    
    /// <summary>
    /// An Area2D around this projectile, used for homing
    /// </summary>
    public Area2D HomingFinder { get; private set; }
    
    private float _life = 0.0f;
    private bool _hasTriggered = false;
    private bool _hasTimered = false;

    public Team Team { get; set; }
    

    [Signal]
    public delegate void TriggerHit(Projectile proj);
    
    [Signal]
    public delegate void TimerHit(Projectile proj);

    [Signal]
    public delegate void ProjectileMove(Projectile proj, float delta);

    [Signal]
    public delegate void ProjectileHit(Projectile proj, StatusArea status);

    private GlobalSound sound = GlobalSound.Instance;
    public override void _Ready()
    {
        base._Ready();
        
        Connect("body_entered", this, nameof(_BodyEntered));
        Connect("area_entered", this, nameof(_AreaEntered));
        sound.base_sound.Playing = true;
        Speed = BaseSpeed;
        
        HomingFinder = FindNode("HomingFinder") as Area2D;
    }

    public override void _PhysicsProcess(float delta)
    {
        base._PhysicsProcess(delta);

        _life += delta;
        if (_life >= Lifetime / 2 && !_hasTimered)
        {
            EmitSignal(nameof(TimerHit), this);
            _hasTimered = true;
        }
        
        foreach (var area in HomingFinder.GetOverlappingAreas())
        {
            if (area is StatusArea status)
            {
                if (status.hasStatus(StatusEffect.statisType.targeted) && status.Team != Team)
                {
                    Direction = (status.GlobalPosition - GlobalPosition).Normalized();
                    break;
                }
            }
        }
        EmitSignal(nameof(ProjectileMove), this, delta);
        
        GlobalPosition += Direction * Speed * delta;
        Rotation = Mathf.Atan2(Direction.y, Direction.x) + Mathf.Pi;

        if (_life >= Lifetime)
            QueueFree();
    }
    
    public void _BodyEntered(Node body)
    {
        if (body is StaticBody2D || body is TileMap)
        {
            EmitSignal(nameof(TriggerHit), this);
            QueueFree();
        }
    }

    public void _AreaEntered(Area2D area)
    {
        if (area.IsInGroup("Shields") && Team != Team.ENEMY)
        {
            EmitSignal(nameof(TriggerHit), this);
            QueueFree();
        }
        if (area is StatusArea status)
        {
            if (status.Team != Team)
            {
                EmitSignal(nameof(TriggerHit), this);
                EmitSignal(nameof(ProjectileHit), this, status);
                status.Damage(Damage, GlobalPosition);
                if (Team == Team.PLAYER)
                {
                    CameraEffects.Instance.Hitstop(0.2f);
                    uint damage = GD.Randi() % 3;
                    if (damage == 0)
                        GlobalSound.Instance.damage1.Play();
                    else if (damage == 1)
                        GlobalSound.Instance.damage2.Play();
                    else if (damage == 2)
                        GlobalSound.Instance.damage3.Play();
                }

                QueueFree();
            }
        }
    }
}
