﻿public class TransitionTile
{
    public enum TileType
    {
        AIR,
        GROUND
    };

    public TileType Type;

    public int FloodFillIndex = 0;

    public bool Ladder = false;
    
    public TransitionTile()
    {
        Type = TileType.GROUND;
    }
}