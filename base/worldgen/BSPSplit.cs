﻿using System.Collections.Generic;
using ExtraMath;
using Godot;

public class BSPSplit
{
    private enum SplitType
    {
        RANDOM, LONG, SHORT
    }
    
    public static List<Rect2i> SplitRegion(Rect2i region, int depth,
        int minWidth, int minHeight,
        float splitPercentShort, float splitPercentLong,
        float mainPortionMin, float mainPortionMax)
    {
        if (region.Size.x <= minWidth || region.Size.y <= minHeight)
        {
            if (region.Size.x > 0 && region.Size.y > 0)
            {
                return new List<Rect2i>(new []{region});
            }

            return new List<Rect2i>();
        }
        
        SplitType type = SplitType.RANDOM;
        float splitRoll = GD.Randf();
        if (splitRoll < splitPercentShort)
            type = SplitType.SHORT;
        else if (splitRoll < splitPercentShort + splitPercentLong)
            type = SplitType.LONG;
        else
            type = SplitType.RANDOM;

        bool splitVertical = false;
        switch (type)
        {
            case SplitType.RANDOM:
                splitVertical = GD.Randf() > 0.5f;
                break;
            case SplitType.LONG:
                splitVertical = region.Size.x > region.Size.y;
                break;
            case SplitType.SHORT:
                splitVertical = region.Size.x < region.Size.y;
                break;
        }

        float portion = (float) GD.RandRange(mainPortionMin, mainPortionMax);
        if (GD.Randf() > 0.5f)
            portion = 1.0f - portion;
        
        List<Rect2i> splits = new List<Rect2i>();
        if (splitVertical)
        {
            int leftWidth = Mathf.FloorToInt(region.Size.x * portion);
            Rect2i left = new Rect2i(region.Position, leftWidth, region.Size.y);
            Rect2i right = new Rect2i(region.Position + new Vector2i(leftWidth, 0), region.Size.x - leftWidth, region.Size.y);
            splits.AddRange(SplitRegion(left, depth + 1,
                minWidth, minHeight,
                splitPercentShort, splitPercentLong,
                mainPortionMin, mainPortionMax));
            splits.AddRange(SplitRegion(right, depth + 1,
                minWidth, minHeight,
                splitPercentShort, splitPercentLong,
                mainPortionMin, mainPortionMax));
        }
        else
        {
            int topHeight = Mathf.FloorToInt(region.Size.y * portion);
            Rect2i top = new Rect2i(region.Position, region.Size.x, topHeight);
            Rect2i bottom = new Rect2i(region.Position + new Vector2i(0, topHeight), region.Size.x, region.Size.y - topHeight);
            splits.AddRange(SplitRegion(top, depth + 1,
                minWidth, minHeight,
                splitPercentShort, splitPercentLong,
                mainPortionMin, mainPortionMax));
            splits.AddRange(SplitRegion(bottom, depth + 1,
                minWidth, minHeight,
                splitPercentShort, splitPercentLong,
                mainPortionMin, mainPortionMax));
        }

        return splits;
    }
    
    public static List<Rect2i> CullRegions(List<Rect2i> regions, Rect2i border,
        float removalChanceEdge, float removalChanceInner)
    {
        List<Rect2i> culled = new List<Rect2i>();
        foreach (var region in regions)
        {
            if (region.Position.x == border.Position.x
                || region.Position.y == border.Position.y
                || region.End.x == border.End.x
                || region.End.y == border.End.y)
            {
                if (GD.Randf() > removalChanceEdge)
                    culled.Add(region);
            }
            else
            {
                if (GD.Randf() > removalChanceInner)
                    culled.Add(region);
            }
        }

        return culled;
    }
}
