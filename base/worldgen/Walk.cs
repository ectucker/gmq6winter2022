﻿using System;
using ExtraMath;
using Godot;

public static class Walk
{
    public static void DrunkenWalk(this TransitionTileMap map,
        Vector2i start, Rect2i bounds,
        Action<TransitionTile> func,
        Func<TransitionTile, bool> count,
        int steps, float hChance)
    {
        int i = 0;
        Vector2i pos = start;
        while (i < steps)
        {
            var tile = map.GetTile(pos);
            if (count(tile))
                i++;
            func(tile);
            
            if (GD.Randf() < hChance)
            {
                
                if (GD.Randf() > 0.5f)
                    pos += Vector2i.Left;
                else
                    pos += Vector2i.Right;
            }
            else
            {
                if (GD.Randf() > 0.5f)
                    pos += Vector2i.Up;
                else
                    pos += Vector2i.Down;
            }

            if (pos.x < bounds.Position.x)
                pos.x = bounds.Position.x;
            if (pos.x >= bounds.End.x)
                pos.x = bounds.End.x - 1;
            if (pos.y < bounds.Position.y)
                pos.y = bounds.Position.y;
            if (pos.y >= bounds.End.y)
                pos.y = bounds.End.y - 1;
        }
    }
}