﻿public class WorldGenParams
{
    // ====
    // Stage parameters
    // ====
    public int WorldPadding = 5;
    
    public int WorldWidth = 120 + 5 * 2;
    public int WorldHeight = 70 + 5 * 2;

    // =====
    // Room parameters
    // =====
    public int RoomMinWidth = 20;
    public int RoomMinHeight = 16;

    public float RoomSplitPercentLong = 0.9f;
    public float RoomSplitPercentShort = 0.05f;
    
    public float RoomMainPortionMin = 0.6f;
    public float RoomMainPortionMax = 0.8f;
    
    public float RoomRemovalChanceEdge = 0.6f;
    public float RoomRemovalChanceInner = 0.4f;
    
    // =====
    // Platform parameters
    // =====
    public int PlatformMinWidth = 2;
    public int PlatformMinHeight = 2;

    public float PlatformSplitPercentLong = 0.9f;
    public float PlatformSplitPercentShort = 0.05f;
    
    public float PlatformMainPortionMin = 0.6f;
    public float PlatformMainPortionMax = 0.8f;
    
    public float PlatformRemovalChanceEdge = 0.6f;
    public float PlatformRemovalChanceInner = 0.8f;
    
    // ====
    // Room edge clearing walk parameters
    // ====
    public int EdgeClearWalkLength = 15;
    public float EdgeClearWalkHChance = 0.5f;
    
    // ====
    // Enemy and other stuff placement parameter
    // ===
    public float EnemyPlaceChance = 0.7f;
    public int EnemyCap = 40;
    public SpawnChance[] Enemies =
    {
        new SpawnChance(0.5f, "res://content/enemies/lizard/lizard.tscn"),
        new SpawnChance(0.7f, "res://content/enemies/wizard/wizard.tscn"),
        new SpawnChance(1.0f, "res://content/enemies/beetle/beetle.tscn"),
        new SpawnChance(1.0f, "res://content/enemies/fire/fire.tscn")
    };
    public float ChestPlaceChance = 0.3f;
    public int LadderMinLength = 7;
    public string LadderTexturePath = "res://base/world/Ladder/Chain.png";
    
    // ====
    // Tileset parameters
    // ====
    public int TopLeftId = 0;
    public int[] TopCenterIds = new int[]{8, 9, 10};
    public int TopRightId = 1;
    public int[] MidIds = new int[]{12, 13, 14, 15};
    public int BotLeftId = 4;
    public int BotCenterId = 6;
    public int BotRightId = 5;
    
    // ====
    // Decor parameters
    // ====
    public float DecorChanceGround = 0.06f;
    public SpawnChance[] DecorGround =
    {
        new SpawnChance(0.5f, "", 2),
        new SpawnChance(0.5f, "", 7)
    };

    public float DecorChanceCeil = 0.1f;
    public SpawnChance[] DecorCeil =
    {
        new SpawnChance(0.5f, "", 3)
    };
}
