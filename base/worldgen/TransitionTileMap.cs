﻿using System;
using ExtraMath;

public class TransitionTileMap
{
    private TransitionTile[,] _tiles;
    public int Width { get; private set; }
    public int Height { get; private set; }
        
    public TransitionTileMap(int width, int height)
    {
        Width = width;
        Height = height;
        _tiles = new TransitionTile[width, height];
        for (int y = 0; y < Height; y++)
        {
            for (int x = 0; x < Width; x++)
            {
                _tiles[x, y] = new TransitionTile();
            }  
        }
    }

    public TransitionTile GetTile(int x, int y)
    {
        return _tiles[x, y];
    }

    public TransitionTile GetTile(Vector2i pos)
    {
        return _tiles[pos.x, pos.y];
    }
    
    public void ForEachTile(Action<Vector2i, TransitionTile> func)
    {
        for (int y = 0; y < Height; y++)
        {
            for (int x = 0; x < Width; x++)
            {
                func(new Vector2i(x, y), _tiles[x, y]);
            }  
        }
    }
    
    public void ForEachTile(Rect2i bounds, Action<Vector2i, TransitionTile> func)
    {
        for (int y = bounds.Position.y; y < bounds.End.y; y++)
        {
            for (int x = bounds.Position.x; x < bounds.End.x; x++)
            {
                func(new Vector2i(x, y), _tiles[x, y]);
            }  
        }
    }

    public bool IsPlatformValid(Rect2i platform)
    {
        Rect2i air = new Rect2i(platform.Position + Vector2i.Up, new Vector2i(platform.Size.x, 1));

        if (air.Position.x < 0 || air.Position.y < 0 || air.End.x >= Width || air.End.y >= Height)
            return false;
        
        for (int y = platform.Position.y; y < platform.End.y; y++)
        {
            for (int x = platform.Position.x; x < platform.End.x; x++)
            {
                if (_tiles[x, y].Type == TransitionTile.TileType.AIR)
                    return false;
            }  
        }
        
        for (int y = air.Position.y; y < air.End.y; y++)
        {
            for (int x = air.Position.x; x < air.End.x; x++)
            {
                if (_tiles[x, y].Type == TransitionTile.TileType.GROUND)
                    return false;
            }  
        }

        return true;
    }
}