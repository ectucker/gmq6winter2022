﻿using System.Collections.Generic;
using ExtraMath;

public static class FloodFill
{
    public static List<FloodFillRegion> IdentifyRegions(this TransitionTileMap map)
    {
        List<FloodFillRegion> regions = new List<FloodFillRegion>();
        
        map.ForEachTile((pos, tile) =>
        {
            if (tile.Type == TransitionTile.TileType.AIR && tile.FloodFillIndex == 0)
                regions.Add(FillRegion(map, pos));
        });

        return regions;
    }

    private static FloodFillRegion FillRegion(TransitionTileMap map, Vector2i pos)
    {
        FloodFillRegion region = new FloodFillRegion();

        Stack<Vector2i> toFill = new Stack<Vector2i>();
        toFill.Push(pos);
        while (toFill.Count > 0)
        {
            pos = toFill.Pop();
            if (pos.x > 0 && pos.y > 0 && pos.x < map.Width && pos.y < map.Height)
            {
                var tile = map.GetTile(pos);
                if (tile.Type == TransitionTile.TileType.AIR && tile.FloodFillIndex == 0)
                {
                    tile.FloodFillIndex = 1;
                    region.Tiles.Add(tile);
                    toFill.Push(pos + Vector2i.Up);
                    toFill.Push(pos + Vector2i.Down);
                    toFill.Push(pos + Vector2i.Left);
                    toFill.Push(pos + Vector2i.Right);
                }
            }
        }

        return region;
    }
}

public class FloodFillRegion
{
    public List<TransitionTile> Tiles = new List<TransitionTile>();
}
