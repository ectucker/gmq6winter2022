﻿public class SpawnChance
{
    public readonly float Chance;
    public readonly string Scene;
    public readonly int Tile;

    public SpawnChance(float chance, string scene = "", int tile = 0)
    {
        Chance = chance;
        Scene = scene;
        Tile = tile;
    }
}