﻿using System;
using System.Collections.Generic;
using System.Linq;
using ExtraMath;
using Godot;
using Troschuetz.Random;
using Troschuetz.Random.Distributions.Continuous;
using Troschuetz.Random.Generators;

public class WorldGenerator : TileMap
{
    private const string PLAYER_PATH = "res://base/entity/player/Player.tscn";
    private const string CHEST_PATH = "res://base/world/LootChest/Chest.tscn";
    private const string LADDER_PATH = "res://base/world/Ladder/Ladder.tscn";
    private const string DOOR_PATH = "res://base/world/Door/Door.tscn";
    
    public WorldGenParams _params = new WorldGenParams();

    private bool _genPressed = false;

    private TransitionTileMap _map;
    
    private void Generate()
    {
        GD.Randomize();
        
        Clear();
        foreach (Node child in GetChildren())
        {
            RemoveChild(child);
        }

        // Create tilemap
        _map = new TransitionTileMap(_params.WorldWidth, _params.WorldHeight);
        Rect2i worldBounds = new Rect2i(_params.WorldPadding, _params.WorldPadding, 
            _params.WorldWidth - _params.WorldPadding * 2,
            _params.WorldHeight - _params.WorldPadding * 2);
        
        // Generate rooms
        List<Rect2i> rooms = BSPSplit.SplitRegion(worldBounds, 0,
            _params.RoomMinWidth, _params.RoomMinHeight,
            _params.RoomSplitPercentShort, _params.RoomSplitPercentLong,
            _params.RoomMainPortionMin, _params.RoomMainPortionMax);
        rooms = BSPSplit.CullRegions(rooms, worldBounds,
            _params.RoomRemovalChanceEdge, _params.RoomRemovalChanceInner);

        // Place rooms in map
        foreach (var room in rooms)
        {
            _map.ForEachTile(room, (Vector2i pos, TransitionTile tile) =>
            {
                tile.Type = TransitionTile.TileType.AIR;
            });
        }
        
        // Generate platforms
        List<Rect2i> allPlatforms = new List<Rect2i>();
        foreach (var room in rooms)
        {
            List<Rect2i> platforms = BSPSplit.SplitRegion(room, 0,
                _params.PlatformMinWidth, _params.PlatformMinHeight,
                _params.PlatformSplitPercentShort, _params.PlatformSplitPercentLong,
                _params.RoomMainPortionMin, _params.RoomMainPortionMax);
            platforms = BSPSplit.CullRegions(platforms, room,
                _params.PlatformRemovalChanceEdge, _params.PlatformRemovalChanceInner);
            allPlatforms.AddRange(platforms);
        }
        
        // Place platforms in map
        foreach (var platform in allPlatforms)
        {
            _map.ForEachTile(platform, (Vector2i pos, TransitionTile tile) =>
            {
                tile.Type = TransitionTile.TileType.GROUND;
            });
        }
        
        // Drunken walk to carve edges of the rooms
        foreach (var room in rooms)
        {
            _map.DrunkenWalk(room.Position + Vector2i.Right * room.Size.x / 2, room, 
                (tile => { tile.Type = TransitionTile.TileType.AIR; }), 
                tile => true,
                _params.EdgeClearWalkLength,
                _params.EdgeClearWalkHChance);
            _map.DrunkenWalk(room.End - new Vector2i(1, 1) + Vector2i.Left * room.Size.x / 2, room, 
                (tile => { tile.Type = TransitionTile.TileType.AIR; }), 
                tile => true,
                _params.EdgeClearWalkLength,
                _params.EdgeClearWalkHChance);
            _map.DrunkenWalk(room.Position + Vector2i.Down * room.Size.y / 2, room, 
                (tile => { tile.Type = TransitionTile.TileType.AIR; }), 
                tile => true,
                _params.EdgeClearWalkLength,
                _params.EdgeClearWalkHChance);
            _map.DrunkenWalk(room.End - new Vector2i(1, 1) + Vector2i.Up * room.Size.y / 2, room, 
                (tile => { tile.Type = TransitionTile.TileType.AIR; }), 
                tile => true,
                _params.EdgeClearWalkLength,
                _params.EdgeClearWalkHChance);
        }
        
        // Fill in small regions
        var regions = _map.IdentifyRegions();
        var maxRegion = regions[0];
        foreach (var region in regions)
        {
            if (region.Tiles.Count > maxRegion.Tiles.Count)
                maxRegion = region;
        }
        foreach (var region in regions)
        {
            if (region != maxRegion)
            {
                foreach (var tile in region.Tiles)
                    tile.Type = TransitionTile.TileType.GROUND;
            }
        }
        
        // Identify valid platforms
        var validPlatforms = (from platform in allPlatforms
            where _map.IsPlatformValid(platform)
            select platform).ToList();
        
        // Find the bottom-left and top-right platforms (start and exit)
        var bottomLeft = validPlatforms[0];
        var topRight = validPlatforms[0];
        foreach (var platform in validPlatforms)
        {
            if (platform.Position.x < bottomLeft.Position.x && platform.Position.y > bottomLeft.Position.y)
                bottomLeft = platform;
            if (platform.Position.x > topRight.Position.x && platform.Position.y < topRight.Position.y)
                topRight = platform;
        }
        validPlatforms.Remove(bottomLeft);
        validPlatforms.Remove(topRight);

        if (topRight == bottomLeft)
        {
            GetTree().ChangeScene(GetTree().CurrentScene.Filename);
            return;
        }

        // Place player
        PlaceOnPlatform(bottomLeft, PLAYER_PATH);
        
        // Place door
        PlaceOnPlatform(topRight, DOOR_PATH);

        // Place random elements
        int spawnedEnemies = 0;
        foreach (var platform in validPlatforms)
        {
            if (GD.Randf() < _params.EnemyPlaceChance && spawnedEnemies < _params.EnemyCap)
            {
                spawnedEnemies++;
                PlaceOnPlatform(platform, RollChance(_params.Enemies).Scene);
            }
            if (GD.Randf() < _params.ChestPlaceChance)
                PlaceOnPlatform(platform, CHEST_PATH);
        }
        
        PlaceLadders();
        
        // Places tiles from map
        _map.ForEachTile((Vector2i pos, TransitionTile tile) =>
        {
            if (tile.Type == TransitionTile.TileType.GROUND)
                SetCell2(pos.x, pos.y, 0);
        });

        // Correct tileset
        foreach (Vector2 cell in GetUsedCells())
        {
            // Top left
            if (GetCellv(cell + Vector2.Left) == -1 && GetCellv(cell + Vector2.Up) == -1)
            {
                SetCellv(cell, _params.TopLeftId);
                RollGroundDecor(cell);
            }
            // Top right
            else if (GetCellv(cell + Vector2.Right) == -1 && GetCellv(cell + Vector2.Up) == -1)
            {
                SetCellv(cell, _params.TopRightId);
                RollGroundDecor(cell);
            }
            // Top
            else if (GetCellv(cell + Vector2.Up) == -1)
            {
                SetCellv(cell, _params.TopCenterIds[GD.Randi() % _params.TopCenterIds.Length]);
                RollGroundDecor(cell);
            }
            // Bottom left
            else if (GetCellv(cell + Vector2.Left) == -1 && GetCellv(cell + Vector2.Down) == -1)
            {
                SetCellv(cell, _params.BotLeftId);
                RollCeilDecor(cell);
            }
            // Bottom right
            else if (GetCellv(cell + Vector2.Right) == -1 && GetCellv(cell + Vector2.Down) == -1)
            {
                SetCellv(cell, _params.BotRightId);
                RollCeilDecor(cell);
            }
            // Bottom
            else if (GetCellv(cell + Vector2.Down) == -1)
            {
                SetCellv(cell, _params.BotCenterId);
                RollCeilDecor(cell);
            }
            // Mid
            else
            {
                SetCellv(cell, _params.MidIds[GD.Randi() % _params.MidIds.Length]);
            }
        }
    }

    public void RollCeilDecor(Vector2 pos)
    {
        if (GD.Randf() < _params.DecorChanceCeil)
            SetCellv(pos + Vector2.Down, RollChance(_params.DecorCeil).Tile);
            
    }

    public void RollGroundDecor(Vector2 pos)
    {
        if (GD.Randf() < _params.DecorChanceGround)
            SetCellv(pos + Vector2.Up, RollChance(_params.DecorGround).Tile);
    }

    private float ChanceSum(SpawnChance[] entites)
    {
        float sum = 0;
        foreach (var chance in entites)
        {
            sum += chance.Chance;
        }

        return sum;
    }

    private SpawnChance RollChance(SpawnChance[] entities)
    {
        float roll = GD.Randf() * ChanceSum(entities);
        float total = 0;
        foreach (var chance in entities)
        {
            total += chance.Chance;
            if (roll < total)
                return chance;
        }

        return entities[entities.Length - 1];
    }

    private void PlaceLadders()
    {
        for (int x = 1; x < _map.Width - 1; x++)
        {
            int emptyTiles = -1000;
            for (int y = _map.Height - 2; y >= 1; y--)
            {
                if (_map.GetTile(x, y).Type == TransitionTile.TileType.AIR)
                {
                    emptyTiles++;
                }
                else
                {
                    emptyTiles = 0;
                }
                
                if (_map.GetTile(x, y - 1).Type == TransitionTile.TileType.AIR)
                {
                    if (_map.GetTile(x + 1, y).Type == TransitionTile.TileType.GROUND)
                    {
                        if (_map.GetTile(x + 1, y - 1).Type == TransitionTile.TileType.AIR)
                        {
                            if (emptyTiles >= _params.LadderMinLength)
                            {
                                if (!_map.GetTile(x - 1, y).Ladder && !_map.GetTile(x + 1, y).Ladder &&
                                    !_map.GetTile(x - 2, y).Ladder && !_map.GetTile(x + 2, y).Ladder)
                                {
                                    var ladder = PlaceOnTile(new Vector2i(x, y + emptyTiles), LADDER_PATH,
                                        new Vector2(96.0f, 0.0f)) as Ladder;
                                    ladder.TexturePath = _params.LadderTexturePath;
                                    ladder.Length = emptyTiles * 2 + 2;
                                    for (int ladderY = y; ladderY < y + emptyTiles + 1; ladderY++)
                                    {
                                        _map.GetTile(x, ladderY).Ladder = true;
                                    }
                                }

                                emptyTiles = -1000;
                            }
                        }
                    }

                    if (_map.GetTile(x - 1, y).Type == TransitionTile.TileType.GROUND)
                    {
                        if (_map.GetTile(x - 1, y - 1).Type == TransitionTile.TileType.AIR)
                        {
                            if (emptyTiles >= _params.LadderMinLength)
                            {
                                if (!_map.GetTile(x - 1, y).Ladder && !_map.GetTile(x + 1, y).Ladder &&
                                    !_map.GetTile(x - 2, y).Ladder && !_map.GetTile(x + 2, y).Ladder)
                                {
                                    var ladder = PlaceOnTile(new Vector2i(x, y + emptyTiles), LADDER_PATH,
                                        new Vector2(32.0f, 0.0f)) as Ladder;
                                    ladder.TexturePath = _params.LadderTexturePath;
                                    ladder.Length = emptyTiles * 2 + 2;
                                    for (int ladderY = y; ladderY < y + emptyTiles + 1; ladderY++)
                                    {
                                        _map.GetTile(x, ladderY).Ladder = true;
                                    }
                                }

                                emptyTiles = -1000;
                            }
                        }
                    }
                }
            }
        }
    }

    private void PlaceOnPlatform(Rect2i platform, string scenePath)
    {
        var scene = GD.Load<PackedScene>(scenePath);
        var node = scene.Instance<Node2D>();
        GetParent().AddChild(node);
        node.GlobalPosition = ToGlobal(MapToWorld((platform.Position + Vector2.Right * platform.Size.x * 0.5f) * 2.0f) + Vector2.Up * 2.0f);
    }
    
    private Node2D PlaceOnTile(Vector2i tile, string scenePath, Vector2 extraOffset)
    {
        var scene = GD.Load<PackedScene>(scenePath);
        var node = scene.Instance<Node2D>();
        GetParent().AddChild(node);
        node.GlobalPosition = ToGlobal(MapToWorld(tile * 2)) + extraOffset;
        return node;
    }

    private void SetCell2(int x, int y, int tile)
    {
        Vector2i pos = new Vector2i(x, y) * 2;
        SetCellv(pos, tile);
        SetCellv(pos + Vector2i.Down, tile);
        SetCellv(pos + Vector2i.Right, tile);
        SetCellv(pos + Vector2i.Down + Vector2i.Right, tile);
    }

    protected virtual void SetupParams() {}
    
    public override async void _Ready()
    {
        base._Ready();

        await ToSignal(GetTree(), "idle_frame");
        SetupParams();
        Generate();
    }

    public override void _Process(float delta)
    {
        base._Process(delta);

        /*if (Input.IsKeyPressed((int) KeyList.R) && !_genPressed)
        {
            _genPressed = true;
            Generate();
        }
        else if (!Input.IsKeyPressed((int) KeyList.R))
        {
            _genPressed = false;
        }*/
    }
}
