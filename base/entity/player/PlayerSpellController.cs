using Godot;
using System;

public class PlayerSpellController : Position2D
{
    private const float SPELL_COOLDOWN = 0.35f;
    private float _timeSinceCast = Mathf.Inf;
    private int _nextSpell = 0;

    private Particles2D _spellFlash;

    private Player _player;
    private SpriteFlip _flip;
    private float _dirTime = 0;

    public override void _Ready()
    {
        base._Ready();

        _spellFlash = GetNode<Particles2D>("SpellFlash");
        foreach (var panel in Inventory.Instance.WandPanels)
        {
            panel.SetSelected(false);
        }
        Inventory.Instance.WandPanels[_nextSpell].SetSelected(true);
        _player = this.FindParent<Player>();
        _flip = this.FindParent<SpriteFlip>();
    }

    public override void _PhysicsProcess(float delta)
    {
        base._PhysicsProcess(delta);

        _timeSinceCast += delta;
        if (Input.IsActionPressed("action_cast") && _timeSinceCast > SPELL_COOLDOWN && !Inventory.Instance.Open)
        {
            _timeSinceCast = 0;
            Inventory.Instance.Spells[_nextSpell].Cast(Team.PLAYER, GlobalPosition, InputUtil.GetWorldMousePosition());
            _nextSpell = Mathf.Wrap(_nextSpell + 1, 0, Inventory.Instance.Spells.Count);
            foreach (var panel in Inventory.Instance.WandPanels)
            {
                panel.SetSelected(false);
            }
            Inventory.Instance.WandPanels[_nextSpell].SetSelected(true);
            CameraEffects.Instance.Trauma += 0.2f;
            CameraEffects.Instance.Kickback((GlobalPosition - InputUtil.GetWorldMousePosition()).Normalized(), 5.0f);
            _spellFlash.Emitting = true;
            _player.OverrideAnim("cast");
            _flip.TempDir = -Mathf.Sign((GlobalPosition - InputUtil.GetWorldMousePosition()).x);
            _dirTime = 0.4f;
        }

        _dirTime -= delta;
        if (_dirTime < 0)
            _flip.TempDir = 0;
    }
}
