using Godot;

public class Player : Entity
{
	private const float JUMP_IMPULSE = 1200.0f;

	private const float COYOTE_TIME = 0.1f;
	private const float JUMP_BUFFER_TIME = 0.1f;

	private const float MAX_HEAD_CORRECTION = 20.0f;
	private const int HEAD_CHECK_ITERATIONS = 10;

	private const float LADDER_SPEED_UP = 12.0f;
	private const float LADDER_SPEED_DOWN = 15.0f;

	public override Vector2 MaxSpeed => new Vector2(310.0f, 0.0f);
	public override float GroundAcceleration => 4.0f;
	public override float AirAcceleration => 4.0f;
	public override float GroundFriction => 10.0f;
	public override float AirFriction => 2.0f;
	
	private float _gravity = 1.0f;
	public override float Gravity => _gravity;

	private RayCast2D _rightHeadCast;
	private RayCast2D _leftHeadCast;
	private RayCast2D _centerHeadCast;
	
	public bool OverlappingLadder { get; set; } = false;
	private bool _grabbedLadder = false;

	private float _lastOnGround = Mathf.Inf;
	private float _jumpLastPressed = Mathf.Inf;
	private float _lastOnLadder = Mathf.Inf;

	private int _jumpIndex = 0;

	private ParticleGroup _jumpParticles;
	private ParticleGroup _landParticles;

	private bool _dead = false;
	private AudioStreamPlayer jumpSound;
	private AudioStreamPlayer footsteps;
	private AudioStreamPlayer damaged;

	private AnimationPlayer _animation;
	public override void _Ready()
	{
		base._Ready();

		_animation = this.FindChild<AnimationPlayer>();
		_rightHeadCast = GetNode<RayCast2D>("RightHeadCast");
		_leftHeadCast = GetNode<RayCast2D>("LeftHeadCast");
		_centerHeadCast = GetNode<RayCast2D>("CenterHeadCast");
		_jumpParticles = GetNode<ParticleGroup>("JumpParticles");
		_landParticles = GetNode<ParticleGroup>("LandParticles");
		jumpSound = GetNode<AudioStreamPlayer>("jump");
		footsteps = GetNode<AudioStreamPlayer>("footsteps");
		damaged = GetNode<AudioStreamPlayer>("player_damaged");
		Status.Connect(nameof(StatusArea.isKilled), this, nameof(Killed));
		Status.Connect(nameof(StatusArea.damaged), this, nameof(damageSound));
		PlayerHealthbar.Player = this;
		GlobalMenus.Instance.ShowMenu("CraftingMenu");
		CompassNeedle.Instance.Player = this;
	}

	private void damageSound()
	{
		damaged.Play();
	}
	public override void _PhysicsProcess(float delta)
	{
		if (!_dead)
		{
			base._PhysicsProcess(delta);

			_lastOnGround += delta;
			_lastOnLadder += delta;
			if (IsOnFloor())
			{
				_lastOnGround = 0;
				_jumpIndex = 0;
				Inventory.Instance.MovementPanel.SetSelection(_jumpIndex);
			}

			_jumpLastPressed += delta;
			if (Input.IsActionJustPressed("move_jump"))
				_jumpLastPressed = 0;

			if ((Input.IsActionJustPressed("move_jump") || Input.IsActionJustPressed("move_down")) && OverlappingLadder && !_grabbedLadder)
			{
				_grabbedLadder = true;
			}

			if (!_grabbedLadder)
			{
				_gravity = 1.0f;

				if ((_lastOnLadder < 0.5 || _lastOnGround < COYOTE_TIME || _jumpIndex > 0) && _jumpLastPressed < JUMP_BUFFER_TIME)
				{
					DoJump();
					
				}
				else if (_lastOnLadder > 0.5 && _lastOnGround > COYOTE_TIME && _jumpIndex == 0)
				{
					_jumpIndex = 1;
					Inventory.Instance.MovementPanel.SetSelection(_jumpIndex);
				}

				if (Velocity.y < 0 && _rightHeadCast.IsColliding() != _leftHeadCast.IsColliding())
				{
					if (_rightHeadCast.IsColliding())
					{
						float headWidth = GetHeadWidth(_rightHeadCast.Position.x, Vector2.Left);
						if (headWidth < MAX_HEAD_CORRECTION)
						{
							GlobalPosition += Vector2.Left * headWidth;
						}
					}
					else if (_leftHeadCast.IsColliding())
					{
						float headWidth = GetHeadWidth(_leftHeadCast.Position.x, Vector2.Right);
						if (headWidth < MAX_HEAD_CORRECTION)
						{
							GlobalPosition += Vector2.Right * headWidth;
						}
					}
				}

				AccelDirX = Input.GetActionStrength("move_right") - Input.GetActionStrength("move_left");
				if (IsOnFloor())
				{
					if (AccelDirX != 0)
						SetAnimIfNotOverride("run");
					else
						SetAnimIfNotOverride("stand");
				}
			}
			else
			{
				SetAnimIfNotOverride("climb");
				_gravity = 0.0f;
				_lastOnGround = Mathf.Inf;
				Velocity = Vector2.Zero;
				AccelDirX = 0;
				AccelDirY = 0;
				_lastOnLadder = 0;

				if (Input.IsActionPressed("move_left") || Input.IsActionJustPressed("move_right"))
				{
					_grabbedLadder = false;
					_lastOnGround = 0;
					_jumpIndex = 0;
				}

				float ladderDir = Input.GetActionStrength("move_down") * LADDER_SPEED_DOWN -
				                  Input.GetActionStrength("move_jump") * LADDER_SPEED_UP;
				if (ladderDir == 0)
				{
					if (_animation.CurrentAnimation == "climb")
						_animation.Stop();
				}
				MoveAndCollide(Vector2.Down * ladderDir);

				if (!OverlappingLadder)
				{
					_grabbedLadder = false;
					_lastOnGround = Mathf.Inf;
					_jumpIndex = 0;
				}
			}
		}
	}

	private float GetHeadWidth(float startX, Vector2 direction)
	{
		_centerHeadCast.Position = new Vector2(startX, _centerHeadCast.Position.x);
		Vector2 iteration = MAX_HEAD_CORRECTION / HEAD_CHECK_ITERATIONS * direction;
		for (int i = 1; i < HEAD_CHECK_ITERATIONS + 1; i++)
		{
			_centerHeadCast.Position += iteration;
			_centerHeadCast.ForceRaycastUpdate();
			if (!_centerHeadCast.IsColliding())
				return MAX_HEAD_CORRECTION / HEAD_CHECK_ITERATIONS * i;
		}

		return MAX_HEAD_CORRECTION * 5;
	}

	private void DoJump()
	{
		if (_jumpIndex <= Inventory.Instance.Movement.Count)
		{
			if (_jumpIndex == 0)
			{
				jumpSound.Play();
				if (VelocityY > 0)
					VelocityY = 0;
				if (VelocityY > -JUMP_IMPULSE)
					VelocityY = -JUMP_IMPULSE;
				_jumpParticles.SetEmitting(true);
				OverrideAnim("jump");
			}
			else
			{
				Inventory.Instance.Movement[_jumpIndex - 1].DoMovement(this);
			}
			
			_jumpIndex++;
			Inventory.Instance.MovementPanel.SetSelection(_jumpIndex);
			_jumpLastPressed = Mathf.Inf;
			
			_lastOnGround = Mathf.Inf;
		}
	}

	public void SetAnimIfNotOverride(string anim)
	{
		if (_animation.CurrentAnimation == "cast" || _animation.CurrentAnimation == "jump")
			return;
		_animation.PlayIfNot(anim);
	}

	public void OverrideAnim(string anim)
	{
		_animation.Play(anim);
	}

	private void Killed()
	{
		Visible = false;
		_dead = true;
		GlobalMenus.Instance.ShowMenu("DeathMenu");
	}
}
