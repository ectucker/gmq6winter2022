using Godot;
using System;

public class PlayerCamera : Camera2D
{
    public override void _Process(float delta)
    {
        base._Process(delta);

        Offset = CameraEffects.Instance.Offset;
    }
}
