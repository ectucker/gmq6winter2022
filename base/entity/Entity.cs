﻿using System.Collections.Generic;
using Godot;

// Represents an entity (something that has physical presence in the world)
// Entities have physics and status (health)
public abstract class Entity : KinematicBody2D
{
    private const string MATERIAL_PATH = "res://base/entity/hitflash_material.tres";
        
    public Vector2 Velocity { get; set; } = Vector2.Zero;
    public float VelocityX
    {
        get => Velocity.x;
        set => Velocity = new Vector2(value, Velocity.y);
    }
    public float VelocityY
    {
        get => Velocity.y;
        set => Velocity = new Vector2(Velocity.x, value);
    }
    
    public Vector2 AccelDir { get; protected set; } = Vector2.Zero;
    public float AccelDirX
    {
        get => AccelDir.x;
        protected set => AccelDir = new Vector2(value, AccelDir.y);
    }
    public float AccelDirY
    {
        get => AccelDir.y;
        protected set => AccelDir = new Vector2(AccelDir.x, value);
    }

    public abstract Vector2 MaxSpeed { get; }
    public float MaxSpeedX => MaxSpeed.x;
    public float MaxSpeedY => MaxSpeed.y;

    public abstract float GroundAcceleration { get; }
    
    public abstract float AirAcceleration { get; }
    
    public abstract float GroundFriction { get; }
    
    public abstract float AirFriction { get; }

    public virtual float Gravity
    {
        get;
        set;
    } = 1.0f;

    public virtual float Mass => 1.0f;
    
    public StatusArea Status { get; private set; }

    public List<Sprite> Sprites { get; private set; }

    public override void _Ready()
    {
        base._Ready();
        StatusArea statusArea = this.FindChild<StatusArea>();
        Status = this.FindChild<StatusArea>();
        Sprites = this.FindChildren<Sprite>();
        foreach (var sprite in Sprites)
        {
            sprite.Material = GD.Load<Material>(MATERIAL_PATH);
            sprite.Material = sprite.Material.Duplicate() as Material;
        }
    }

    public override void _PhysicsProcess(float delta)
    {
        base._PhysicsProcess(delta);
        
        float frictionConstX = IsOnFloor() ? GroundFriction : AirFriction;
        float frictionConstY = AirFriction; // Y direction is always air

        float accelConstX = IsOnFloor() ? GroundAcceleration : AirAcceleration;
        float accelConstY = AirAcceleration;
        
        // Horizontal acceleration
        float speedX = Mathf.Abs(Velocity.x);
        float speedLimitX = Mathf.Max(speedX, MaxSpeedX);
        float accelX = AccelDirX * MaxSpeedX * accelConstX;
        float frictionX = Velocity.x * frictionConstX;
        if (speedX < MaxSpeedX)
        {
            // If friction is resisting acceleration, don't
            if (Mathf.Sign(frictionX) == Mathf.Sign(accelX))
                frictionX = 0;
        }
        VelocityX += accelX * delta;
        VelocityX -= frictionX * delta;
        VelocityX = Mathf.Clamp(VelocityX, -speedLimitX, speedLimitX);
        
        // Vertical acceleration
        float speedY = Mathf.Abs(Velocity.y);
        float speedLimitY = Mathf.Max(speedY, MaxSpeedY);
        float accelY = AccelDirY * MaxSpeedY * accelConstY;
        float frictionY = MaxSpeedY * frictionConstY;
        if (speedY < MaxSpeedY)
        {
            // If friction is resisting acceleration, don't
            if (Mathf.Sign(frictionY) == Mathf.Sign(accelY))
                frictionY = 0;
        }
        VelocityY += accelY * delta;
        //VelocityY -= frictionY * delta;
        VelocityY = Mathf.Clamp(VelocityY, -speedLimitY, speedLimitY);
        
        // Gravity
        VelocityY += PhysicsConsts.GRAVITY * Gravity * delta;
        
        Velocity = MoveAndSlide(Velocity, Vector2.Up, false, 2);
    }

    public void ApplyImpulse(Vector2 impulse)
    {
        Velocity += impulse / Mass;
    }
}
