﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Eventing.Reader;
using System.Threading.Tasks;
using Godot;

public class StatusArea : Area2D
{
    private const string FLOATING_TEXT_SCENE = "res://base/entity/float_text.tscn";
    
    [Export] private int _team = 0;
    [Export] private float _maxHealth = 100;

    [Signal] public delegate void isKilled();
    [Signal] public delegate void damaged();
    public Team Team;
    private float time = 0;
    public float MaxHealth { get; set; }
    public float Health { get; private set; }

    private List<StatusEffect> _statusEffects = new List<StatusEffect>();
    
    public Entity Entity { get; set; }

    public bool Invincible = false;

    
    public override void _Ready()
    {
        base._Ready();

        Team = _team > 0 ? Team.PLAYER : Team.ENEMY;
        Entity = this.FindParent<Entity>();
        Health = _maxHealth;
        MaxHealth = _maxHealth;
        
        //add signal to run addStatusEffects
        
    }

    public void Damage(float amount, Vector2 source)
    {
        if (!Invincible)
        {
            Health -= amount;
            if (Health < 0)
            {
                EmitSignal((nameof(isKilled)));
            }

            EmitSignal(nameof(damaged));
            HitflashAnimation();

            Entity.ApplyImpulse(Vector2.Up * amount * 20.0f);

            if (Team == Team.PLAYER)
            {
                CameraEffects.Instance.Trauma += 1.0f;
            }
            else
            {
                CameraEffects.Instance.Trauma += 0.2f;
                var text = GlobalLocations.SpawnRoot.Spawn<FloatingText>(FLOATING_TEXT_SCENE);
                text.GlobalPosition = GlobalPosition +
                                      new Vector2((float) GD.RandRange(-20, 20), (float) GD.RandRange(-20, 20));
                text.Float($"{(amount * 10):f0}", 1.0f);
            }
        }
    }

    private async Task HitflashAnimation()
    {
        SetHitflash(Colors.White);
        await Awaiters.Delay(0.07f);
        SetHitflash(Colors.Black);
        await Awaiters.Delay(0.05f);
        SetHitflash(Colors.White);
        await Awaiters.Delay(0.07f);
        SetHitflash(Colors.Black);
        await Awaiters.Delay(0.05f);
        SetHitflash(Colors.Black);
    }

    private void SetHitflash(Color color)
    {
        foreach (var sprite in Entity.Sprites)
        {
            if (sprite.Material is ShaderMaterial shader)
                shader.SetShaderParam("add_color", color);
        }
    }

    public override void _PhysicsProcess(float delta)
    {
       checkStatusClear(delta);

       if (hasStatus(StatusEffect.statisType.freeze))
           Entity.Velocity = new Vector2(0, 0);



    }

    //addStatusEffects, allows us to just connect the signal to this and can easily add more effects
    public void addStatusEffects(StatusEffect.statisType effect, float time, float var)
    {
       _statusEffects.Add(new StatusEffect(effect,time,var));
       switch (effect)
       {
           
           case StatusEffect.statisType.levitate:
               Entity.Gravity = 0.0f;
               Entity.ApplyImpulse(Vector2.Up * 50.0f);
              break;
           case StatusEffect.statisType.freeze:
               Entity.Velocity = new Vector2(0, 0);
               break;
           case StatusEffect.statisType.weight:
               Entity.Gravity = 10f;
               break;
       }
    }

    private void checkStatusClear(float t)
    {
        List<StatusEffect> toRemove = new List<StatusEffect>();
        foreach (var effect in _statusEffects)
        {
            effect.updateTime(t);
            if (effect.timeUp())
                toRemove.Add((effect));
        }

        foreach (var effect in toRemove)
        {
            _statusEffects.Remove(effect);
        }
        
        foreach (var effect in toRemove)
        {
            if (!hasStatus(effect._effect))
            {
                switch (effect._effect)
                {
                    case StatusEffect.statisType.levitate:
                        Entity.Gravity = 1.0f;
                        break;
                    case StatusEffect.statisType.weight:
                        Entity.Gravity = 1.0f;
                        break;
                }
            }
        }
    }
    //addStatusEffects can run other methods just to keep the switch clean

    public bool hasStatus(StatusEffect.statisType status)
    {
        foreach (var effect in _statusEffects)
        {
            if (effect._effect == status)
                return true;
        }
        return false;
    }

}
