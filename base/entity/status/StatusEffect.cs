﻿
    public class StatusEffect
    {
       public enum statisType
        {
            fire, freeze, levitate, weight, targeted
        }
        public statisType _effect { get; private set; }
        public float _time { get; private set; }
        public float _lifeTime { get; private set; }
        public float _strength { get; private set; }
        
        public StatusEffect(statisType s, float life, float str)
        {
            _effect = s;
            _time = 0;
            _lifeTime = life;
            _strength = str;
        }

        public void updateTime(float delta)
        {
            _time += delta;
        }

        public bool timeUp()
        {
            if (_time > _lifeTime)
            {
                return true;
            }

            return false;
        }
    }
