using Godot;
using System;

public class FloatingText : Node2D
{
    private Label _label;
    private float _lifetime;
    private float _time = 0;
    
    public override void _Ready()
    {
        _label = this.GetNode<Label>("Label");
    }

    public void Float(string text, float time)
    {
        _label.Text = text;
        _lifetime = time;
    }

    public override void _Process(float delta)
    {
        base._Process(delta);

        GlobalPosition += Vector2.Up * 2.0f;
        
        _time += delta;
        if (_time > _lifetime)
            QueueFree();
    }
}
