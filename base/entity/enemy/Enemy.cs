﻿using Godot;

public abstract class Enemy : Entity
{
    public PlayerFinder PlayerFinder { get; private set; }
    
    protected AnimationPlayer _animation;

    public override void _Ready()
    {
        base._Ready();

        PlayerFinder = this.FindChild<PlayerFinder>();
        _animation = this.FindChild<AnimationPlayer>();
        
        Status.Connect(nameof(StatusArea.isKilled), this, nameof(Kill));
    }

    private void Kill()
    {
        QueueFree();
    }
}
