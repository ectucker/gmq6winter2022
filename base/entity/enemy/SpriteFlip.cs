using Godot;
using System;
using System.Runtime.Remoting.Metadata.W3cXsd2001;

public class SpriteFlip : Sprite
{

    Entity e;
    public float TempDir = 0;
    private float _baseScale;

    public override void _Ready()
    {
        base._Ready();
        Node n = this.FindParent<Entity>();
        e = (Entity)n;
        _baseScale = Scale.x;
    }


   

    public override void _Process(float delta)
    {
        var dir = e.AccelDirX;
        if (TempDir != 0)
            dir = TempDir;
      if (dir > 0f)
      {
          Scale = new Vector2(-_baseScale, Scale.y);
      }
      else if (dir < 0f)
      {
          Scale = new Vector2(_baseScale, Scale.y);
      }
  }
}
