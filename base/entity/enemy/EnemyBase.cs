﻿
using System.Threading.Tasks;
using Godot;

public abstract class EnemyBase : Enemy
{
    /// <summary>
    /// The maximum speed of the enemy, in pixels per second.
    /// </summary>
    public override Vector2 MaxSpeed => new Vector2(310.0f, 0.0f);
    /// <summary>
    /// Ground acceleration of the enemy, as a factor of MaxSpeed.
    /// </summary>
    public override float GroundAcceleration => 4.0f;
    /// <summary>
    /// Air acceleration of the enemy, as a factor of MaxSpeed.
    /// </summary>
    public override float AirAcceleration => 4.0f;
    /// <summary>
    /// Ground friction of the enemy, as a factor of their current speed.
    /// </summary>
    public override float GroundFriction => 10.0f;
    /// <summary>
    /// Ground friction of the enemy, as a factor of MaxSpeed.
    /// </summary>
    public override float AirFriction => 2.0f;
    /// <summary>
    /// Ground friction of the enemy, in pixels per second * kg.
    /// </summary>
    public virtual float JumpImpulse => 1200.0f;
    /// <summary>
    /// The time the enemy will wait between attacking the player and returning to an idle/pursue state.
    /// </summary>
    protected virtual float HitCooldown => 1.0f;
    /// <summary>
    /// The minimum distance the enemy wants to be from the player when pursing.
    /// </summary>
    protected virtual float PlayerTargetRangeMin => 32.0f;
    /// <summary>
    /// The maximum distance the enemy wants to be from the player when pursuing.
    /// </summary>
    protected virtual float PlayerTargetRangeMax => 64.0f;
    
    protected enum AIState
    {
        IDLE, PURSUE, ATTACK, POSTATTACK, RECOVER
    }
    protected AIState State;
    
    private RayCast2D _leftCast;
    private RayCast2D _rightCast;
    
    private float _lastSeenPlayer = Mathf.Inf;
    private float _lastHitPlayer = Mathf.Inf;
    private float _lastAttack = Mathf.Inf;

    private Vector2 _lastVel = Vector2.Zero;

    protected virtual void IdleState()
    {
        SetIdleAccel();
    }

    protected virtual void PursueState()
    {
        SetTargetAccel(PlayerFinder.GetPlayer().GlobalPosition, PlayerTargetRangeMin, PlayerTargetRangeMax);
        if (ShouldAttack())
            DoAttack();
    }

    protected virtual void PostAttackState()
    {
        SetStopAccel();
    }
    
    protected virtual void RecoverState()
    {
        SetStopAccel();
    }
    
    protected abstract void PlayerHitEffect(StatusArea status, int amount);

    protected abstract bool ShouldAttack();
    
    protected abstract Task Attack();
    
    public override void _Ready()
    {
        base._Ready();

        DamageBox damageBox = this.FindChild<DamageBox>();
        damageBox.Connect(nameof(DamageBox.Hit), this, nameof(PlayerHit));
        
        _leftCast = GetNode<RayCast2D>("leftRay");
        _rightCast = GetNode<RayCast2D>("rightRay");
    }
    
    public override void _PhysicsProcess(float delta)
    {
        base._PhysicsProcess(delta);

        // Memory data
        _lastHitPlayer += delta;
        _lastSeenPlayer += delta;
        _lastAttack += delta;
        if (PlayerFinder.HasPlayer())
            _lastSeenPlayer = 0;
        
        // State transitions
        switch (State)
        {
            case AIState.IDLE:
                if (PlayerFinder.HasPlayer())
                    State = AIState.PURSUE;
                break;
            case AIState.PURSUE:
                if (!PlayerFinder.HasPlayer())
                    State = AIState.RECOVER;
                break;
            case AIState.RECOVER:
                if (PlayerFinder.HasPlayer())
                    State = AIState.PURSUE;
                else if (_lastSeenPlayer > HitCooldown)
                    State = AIState.IDLE;
                break;
            case AIState.POSTATTACK:
                if (_lastAttack > HitCooldown)
                    State = AIState.IDLE;
                break;
        }

        // State actions
        switch (State)
        {
            case AIState.IDLE:
                IdleState();
                break;
            case AIState.PURSUE:
                PursueState();
                break;
            case AIState.POSTATTACK:
                PostAttackState();
                break;
            case AIState.RECOVER:
                RecoverState();
                break;
        }
        
        _lastVel = Velocity;
    }
    
    //Player hit code, when the enemy hits player can update behavior
    private void PlayerHit(StatusArea status, int amount)
    {
        _lastHitPlayer = 0;
        _lastAttack = 0;
        State = AIState.POSTATTACK;
        //effect after player is hit
        PlayerHitEffect(status, amount);
    }

    protected bool OnEdgeLeft() => !_leftCast.IsColliding();

    protected bool OnEdgeRight() => !_rightCast.IsColliding();

    protected virtual void SetTargetAccel(Vector2 pos, float distanceMin, float distanceMax)
    {
        if (Mathf.Abs(GlobalPosition.x - pos.x) > distanceMax)
        {
            AccelDirX = -Mathf.Sign(GlobalPosition.x - pos.x);
        }
        else if (Mathf.Abs(GlobalPosition.x - pos.x) < distanceMin)
        {
            AccelDirX = Mathf.Sign(GlobalPosition.x - pos.x);
        }
        else
        {
            AccelDirX = 0.0f;
        }

        if (OnEdgeLeft() || OnEdgeRight() || IsOnWall())
            Jump();
    }

    protected void SetIdleAccel()
    {
        if (AccelDirX == 0)
            if (GD.Randf() > 0.5f)
                AccelDirX = 1.0f;
            else
                AccelDirX = -1.0f;
        
        if (IsOnWall())
        {
            AccelDirX = -Mathf.Sign(_lastVel.x);
            if (AccelDirX == 0)
                AccelDirX = -1.0f;
        }
        
        if (OnEdgeLeft())
        {
            AccelDirX = 1.0f;
        }
        else if (OnEdgeRight())
        {
            AccelDirX = -1.0f;
        }
    }

    protected void SetStopAccel()
    {
        AccelDir = Vector2.Zero;
    }
    
    private void Jump()
    {
        if (IsOnFloor())
        {
            ApplyImpulse(Vector2.Up * JumpImpulse);
        }
    }

    private async Task DoAttack()
    {
        State = AIState.ATTACK;
        await Attack();
        State = AIState.POSTATTACK;
        _lastAttack = 0;
    }
}
