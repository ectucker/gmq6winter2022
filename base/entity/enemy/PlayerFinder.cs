﻿using System.Collections.Generic;
using Godot;

public class PlayerFinder : Area2D
{
    public List<Player> Players { get; private set; } = new List<Player>();

    public override void _Ready()
    {
        base._Ready();

        Connect("body_entered", this, nameof(_BodyEntered));
        Connect("body_exited", this, nameof(_BodyExited));
    }

    private void _BodyEntered(PhysicsBody2D body)
    {
        if (body is Player player)
            Players.Add(player);
    }
    
    private void _BodyExited(PhysicsBody2D body)
    {
        if (body is Player player)
        {
            if (Players.Contains(player))
                Players.Remove(player);
        }
    }

    public bool HasPlayer() => Players.Count > 0;

    public Player GetPlayer() => Players[0];
}
