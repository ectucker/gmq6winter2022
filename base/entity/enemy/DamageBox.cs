using Godot;
using System;

public class DamageBox : Area2D
{
    [Export] private int damage = 10;
    
    [Signal]
    public delegate void Hit(StatusArea status, int amount);
    
    private void _AreaEntered(Area2D a)
    {
        
        if (a is StatusArea status && status.Team == Team.PLAYER)
        {
            status.Damage(damage, GlobalPosition);
            EmitSignal(nameof(Hit), new object[]{status, damage});
        }
        
        
    }
    
    public override void _Ready()
    {
        
        Connect("area_entered", this, nameof(_AreaEntered));
    }

//  // Called every frame. 'delta' is the elapsed time since the previous frame.
//  public override void _Process(float delta)
//  {
//      
//  }
}
