using Godot;
using System;

public class MainMenu : Control
{
    public override void _Ready()
    {
        (FindNode("StartButton") as Button).Connect("pressed", this, nameof(Start));
        (FindNode("ExitButton") as Button).Connect("pressed", this, nameof(Exit));
    }

    public void Start()
    {
        Transitions.Instance.TransitionTo("res://content/biomes/firepits/firepits.tscn");
        GameState.Instance.Reset();
    }

    public void Exit()
    {
        GetTree().Quit();
    }
}