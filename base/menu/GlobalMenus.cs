using Godot;
using System;

public class GlobalMenus : CanvasLayer
{
    public static GlobalMenus Instance { get; private set; }

    public override void _Ready()
    {
        base._Ready();

        Instance = this;
    }

    public void ShowMenu(string menu)
    {
        foreach (Control control in GetChildren())
        {
            control.Visible = control.Name == menu;
        }
    }
}
