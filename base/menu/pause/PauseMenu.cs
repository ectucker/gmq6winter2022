using Godot;
using System;

public class PauseMenu : Control
{
    public override void _Ready()
    {
        (FindNode("RestartButton") as Button).Connect("pressed", this, nameof(Restart));
        (FindNode("ExitButton") as Button).Connect("pressed", this, nameof(Exit));
    }

    public void Restart()
    {
        Transitions.Instance.TransitionTo("res://content/biomes/firepits/firepits.tscn");
        Visible = false;
        GetTree().Paused = false;
        Inventory.Instance.Reset();
        GameState.Instance.Reset();
    }

    public void Exit()
    {
        GetTree().Quit();
    }

    public override void _Input(InputEvent inputEvent)
    {
        base._Input(inputEvent);

        if (inputEvent.IsActionPressed("ui_cancel"))
        {
            Visible = !Visible;
            GetTree().Paused = !GetTree().Paused;
        }

        if (inputEvent.IsActionPressed("action_fullscreen"))
        {
            OS.WindowFullscreen = !OS.WindowFullscreen;
        }
    }
}