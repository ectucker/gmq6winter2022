using Godot;
using System;

public class DeathMenu : Control
{
    public override void _Ready()
    {
        (FindNode("RestartButton") as Button).Connect("pressed", this, nameof(Restart));
        (FindNode("ExitButton") as Button).Connect("pressed", this, nameof(Exit));
    }

    public void Restart()
    {
        Transitions.Instance.TransitionTo("res://content/biomes/firepits/firepits.tscn");
        GameState.Instance.Reset();
        Inventory.Instance.Reset();
        GameState.Instance.Reset();
    }

    public void Exit()
    {
        GetTree().Quit();
    }
}
