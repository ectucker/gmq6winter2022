using Godot;
using System;
using System.Collections.Generic;

public class Inventory : Control
{
    public static Inventory Instance { get; private set; }

    public List<Spell> Spells { get; private set; } = new List<Spell>();

    public List<MovementComponent> Movement { get; private set; } = new List<MovementComponent>();

    public MovementPanel MovementPanel;
    public List<WandPanel> WandPanels;
    private Control _inventorySlots;
    private Control _inventorySection;

    public bool Open { get; private set; } = false;
    
    public override void _Ready()
    {
        base._Ready();

        Instance = this;
        MovementPanel = this.FindChild<MovementPanel>();
        WandPanels = this.FindChildren<WandPanel>();
        _inventorySection = FindNode("InventorySection") as Control;
        _inventorySlots = FindNode("InventorySlots") as Control;
        UpdateSpells();
       
        Reset();
        //ADD ONE OF EACH SPELL AT START
        /*String[] loot = new string[] {"AggressiveLaunch", "hook_shot", "levitate", "magical_mastery", "packed_punch", "precision_shot", "quasar", "sloth_launch", "slug", "TargetLock", "touch_of_frost", "unstable_arcana", "weight", "Leap", "Phase", "Summon Earth"};
        foreach (var s in loot)
        {
            AddSpell(SpellTokens.MakeToken(s));
        }
        AddSpell(SpellTokens.MakeToken("Dash"));
        AddSpell(SpellTokens.MakeToken("Platform"));*/
    }

    public void Reset()
    {
        var movements = MovementPanel.FindChildren<SpellSlot>();
        foreach (var movement in movements)
        {
            movement.Token = SpellTokens.MakeToken("Null");
        }
        movements[0].Token = SpellTokens.MakeToken("Leap");
        foreach (var wand in WandPanels)
        {
            foreach (var slot in wand.FindChildren<SpellSlot>())
            {
                slot.Token = SpellTokens.MakeToken("Null");
            }
        }
        WandPanels[0]._slots[0].Token = SpellTokens.MakeToken("AggressiveLaunch");
        WandPanels[0].Shortend = true;
        WandPanels[1]._slots[0].Token = SpellTokens.MakeToken("sloth_launch");
        WandPanels[1].Shortend = true;
        WandPanels[2]._slots[0].Token = SpellTokens.MakeToken("quasar");
        WandPanels[2].Shortend = true;

        foreach (var slot in _inventorySlots.FindChildren<SpellSlot>())
        {
            slot.Token = SpellTokens.MakeToken("Null");
        }
        UpdateSpells();
    }

    public void UpdateSpells()
    {
        Spells.Clear();
        foreach (var wand in WandPanels)
        {
            Spells.Add(wand.CreateSpell());
        }

        Movement = MovementPanel.CreateMovement();
    }

    public void AddSpell(SpellToken token)
    {
        foreach (SpellSlot slot in _inventorySlots.GetChildren())
        {
            if (slot.Token.Name == "Null")
            {
                slot.Token = token;
                break;
            }
        }
    }

    public override void _Process(float delta)
    {
        base._Process(delta);

        Open = Input.IsActionPressed("action_inventory");
        _inventorySection.Visible = Open;
        foreach (var panel in WandPanels)
        {
            panel.Shortend = !Open;
        }
    }
}
