using Godot;
using System;

public class PlayerHealthbar : TextureProgress
{
    public static Player Player { get; set; }

    public override void _Process(float delta)
    {
        base._Process(delta);

        if (Player != null && IsInstanceValid(Player))
            Value = Player.Status.Health;
    }
}
