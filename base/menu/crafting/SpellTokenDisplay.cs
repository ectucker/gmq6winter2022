using Godot;
using System;

public class SpellTokenDisplay : TextureRect
{
    private SpellToken _token;
    public SpellToken Token
    {
        get => _token;
        set
        {
            _token = value;
            Texture = GD.Load<Texture>(_token.IconPath);
        }
    }

    private float _time = 0;
    
    public override void _Process(float delta)
    {
        base._Process(delta);

        
        if (Inventory.Instance.Open)
        {
            if (_time < 0)
                _time = 0;
            _time += delta;
            RectRotation = 1.0f * Mathf.Sin(_time * 7.0f);
        }
        else
        {
            _time = -1;
        }
    }
}
