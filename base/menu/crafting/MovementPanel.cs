using Godot;
using System.Collections.Generic;
using System.Linq;
using Godot.Collections;

public class MovementPanel : PanelContainer
{
    private List<SpellSlot> _slots;
    private Array _indicators;

    public override void _Ready()
    {
        base._Ready();

        _slots = this.FindChildren<SpellSlot>();
        _indicators = GetNode("Indicators").GetChildren();
    }
    
    public List<MovementComponent> CreateMovement()
    {
        var components = from slot in _slots
            where slot.Token.CreateComponent() is MovementComponent movement
            select slot.Token.CreateComponent() as MovementComponent;
        return components.ToList();
    }

    public void SetSelection(int selection)
    {
        int pos = SelectionToPos(selection);
        for (int i = 0; i < _indicators.Count; i++)
        {
            (_indicators[i] as Control).Visible = pos == i;
        }
    }

    private int SelectionToPos(int selection)
    {
        selection = Mathf.Clamp(selection, 1, 20);
        int counted = 0;
        for (int i = 0; i < _slots.Count; i++)
        {
            if (_slots[i].Token.Name != "Null")
                counted++;
            if (counted == selection)
                return i;
        }

        return -1;
    }
}