using Godot;
using System;
using System.Collections.Generic;
using System.Linq;

public class WandPanel : PanelContainer
{
    public List<SpellSlot> _slots;

    private Control _shortenedLayout;
    private Control _fullLayout;
    private SpellSlot _shortenedSlot;
    private Control _selector;
    
    private bool _shortend = true;
    public bool Shortend
    {
        get => _shortend;
        set
        {
            _shortend = value;
            _shortenedLayout.Visible = _shortend;
            _fullLayout.Visible = !_shortend;
            _shortenedSlot.Token = _slots[0].Token;
        }
    }

    public override void _Ready()
    {
        base._Ready();

        _shortenedLayout = FindNode("ShortenedLayout") as Control;
        _fullLayout = FindNode("Layout") as Control;
        _shortenedSlot = _shortenedLayout.FindChild<SpellSlot>();
        _slots = _fullLayout.FindChildren<SpellSlot>();
        _selector = GetNode<Control>("Indicator");

        Shortend = _shortend;
    }

    public Spell CreateSpell()
    {
        var components = from slot in _slots
            where slot.Token.Name != "Null"
            select slot.Token.CreateComponent();
        return SpellParser.ParseSpell(components.ToList());
    }

    public void SetSelected(bool selected)
    {
        _selector.Visible = selected;
    }
}
