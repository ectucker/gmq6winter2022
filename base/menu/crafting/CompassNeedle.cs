using Godot;
using System;

public class CompassNeedle : Sprite
{
    public static CompassNeedle Instance;
    
    public Player Player { get; set; }
    
    public Door Door { get; set; }
    
    public override void _Ready()
    {
        Instance = this;
    }

    public override void _Process(float delta)
    {
        base._Process(delta);

        if (IsInstanceValid(Door) && IsInstanceValid(Player))
        {
            Vector2 dist = (Door.GlobalPosition - Player.GlobalPosition).Normalized();
            GlobalRotation = Mathf.Atan2(dist.y, dist.x) + Mathf.Pi / 2;
        }
    }
}
