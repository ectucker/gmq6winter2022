using Godot;

public class SpellSlot : Control
{
    private const string TOKEN_DISPLAY_PATH = "res://base/menu/crafting/spell_token.tscn";

    private PackedScene _tokenDisplayScene;

    [Export] private string _startingSpell = "Null";

    [Export] private bool MovementOnly = false;
    [Export] private bool SpellOnly = false;

    private SpellToken _token;
    public SpellToken Token
    {
        get => _token;
        set
        {
            _token = value;
            _display.Token = _token;
            if (Token.Name != "Null")
                HintTooltip = Token.Name;
            else
                HintTooltip = null;
        }
    }
    
    private SpellTokenDisplay _display;

    private AudioStreamPlayer _drag;
    private AudioStreamPlayer _drop;

    public override void _Ready()
    {
        base._Ready();

        _tokenDisplayScene = GD.Load<PackedScene>(TOKEN_DISPLAY_PATH);
        _display = this.FindChild<SpellTokenDisplay>();
        Token = SpellTokens.MakeToken(_startingSpell);
        _drag = GetNode<AudioStreamPlayer>("drag_spell");
        _drop = GetNode<AudioStreamPlayer>("drop_spell");
    }

    public override object GetDragData(Vector2 position)
    {
        if (_token != null && _token.Name != "Null")
        {
            var preview = _tokenDisplayScene.Instance<SpellTokenDisplay>();
            preview.Token = Token;
            SetDragPreview(preview);
            _drag.Play();
            
            return new SpellDragData(this);
        }

        return null;
    }
    
    public override bool CanDropData(Vector2 position, object item)
    {
        if (item is SpellDragData data)
        {
            if (MovementOnly)
            {
                return data.Source.Token.CreateComponent() is MovementComponent;
            }
            else if (SpellOnly)
            {
                return !(data.Source.Token.CreateComponent() is MovementComponent);
            }
            else
            {
                return true;
            }
        }

        return false;
    }

    public override void DropData(Vector2 position, object item)
    {
        if (item is SpellDragData data)
        {
            var temp = Token;
            Token = data.Source.Token;
            data.Source.Token = temp;
            Inventory.Instance.UpdateSpells();
            _drop.Play();
        }
    }

    private class SpellDragData : Reference
    {
        public SpellSlot Source;

        public SpellDragData(SpellSlot source)
        {
            Source = source;
        }
    }

    public override Control _MakeCustomTooltip(string forText)
    {
        var tooltip = GD.Load<PackedScene>("res://base/menu/crafting/spell_tooltip.tscn").Instance<Control>();
        (tooltip.FindNode("Name") as Label).Text = _token.Name;
        (tooltip.FindNode("Desc") as Label).Text = _token.Description;
        (tooltip.FindNode("Flavor") as Label).Text = _token.Flavor;
        return tooltip;
    }
}
