using Godot;
using System;

public class SpawnRoot : Node2D
{
    public override void _Ready()
    {
        GlobalLocations.SpawnRoot = this;
    }

    public T Spawn<T>(string scene) where T : Node
    {
        var node = GD.Load<PackedScene>(scene).Instance();
        AddChild(node);
        return node as T;
    }
}
