using Godot;
using System;
using System.Collections.Generic;

public class Ladder : Area2D
{
    public string TexturePath = "res://base/world/Ladder/Ladder.png";
    private int _length = 5;

    public int Length
    {
        get => _length;
        set
        {
            _length = value;
            CollisionShape2D ladder = (CollisionShape2D) GetNode("LadderHitBox");
            RectangleShape2D hitArea = new RectangleShape2D();
            hitArea.Extents = new Vector2(30, 32 * Length);
            ladder.Shape = hitArea;
            ladder.GlobalPosition = new Vector2(GlobalPosition.x, GlobalPosition.y - (32 * Length));
            for (int i = 0; i < Length; i++)
            {
                Sprite s = new Sprite();
                AddChild(s);
                Texture img = (Texture) GD.Load(TexturePath);
                s.Texture = img;
                s.Scale = new Vector2(0.5f,0.5f);
                s.GlobalPosition = new Vector2(GlobalPosition.x, GlobalPosition.y - (64 * i)-32);
            }
        }
    }
    
   
    // Declare member variables here. Examples:
    // private int a = 2;
    // private string b = "text";
    
    // Called when the node enters the scene tree for the first time.
    public override void _Ready()
    {
        base._Ready();
        Connect("body_entered", this, nameof(_BodyEntered));
        Connect("body_exited", this, nameof(_BodyExited));
    }
    private void _BodyEntered(PhysicsBody2D body)
    {
        if (body is Player player)
        {
           
            player.OverlappingLadder = true;
        }
    }
    
    private void _BodyExited(PhysicsBody2D body)
    {
        if (body is Player player)
        {
            player.OverlappingLadder = false;
        }
    }

    

}
