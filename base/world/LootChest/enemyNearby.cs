using Godot;
using System;
using System.Collections.Generic;

public class enemyNearby : Area2D
{
    // Declare member variables here. Examples:
    // private int a = 2;
    // private string b = "text";
    
    
    
    public List<Entity> ChestEnemies { get; private set; } = new List<Entity>();

    public override void _Ready()
    {
        base._Ready();

        Connect("body_entered", this, nameof(_BodyEntered));
        Connect("body_exited", this, nameof(_BodyExited));
    }

    private void _BodyEntered(PhysicsBody2D body)
    {
        if (body is Entity enemy)
            if(enemy.Status.Team == Team.ENEMY){
                ChestEnemies.Add(enemy);
            }
    }
    
    private void _BodyExited(PhysicsBody2D body)
    {
        if (body is Entity entity)
        {
            if (ChestEnemies.Contains(entity))
                ChestEnemies.Remove(entity);
        }
    }

    public bool hasEnemy()
    {
        if (ChestEnemies != null)
        {
            if (ChestEnemies.Count > 0)
            {
                return true;
            }
        }

        return false;
    }
    
    
}


