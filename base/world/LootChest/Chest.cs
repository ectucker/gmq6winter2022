using Godot;
using System;
using System.Collections.Generic;

public class Chest : Area2D
{
    // Declare member variables here. Examples:
    // private int a = 2;
    // private string b = "text";
    public enemyNearby enemyNearby { get; private set; }
    private SpellToken lootItem = SpellTokens.MakeToken("Null");
    

    // Called when the node enters the scene tree for the first time.
    private Particles2D red;
    private Particles2D green;
    private AudioStreamPlayer _openSound;
    private AnimationPlayer _animation;
    private bool _opened = false;
    String[] loot = new string[] {"AggressiveLaunch", "hook_shot", "levitate", "magical_mastery", "packed_punch", "precision_shot", "quasar", "sloth_launch", "slug", "TargetLock", "touch_of_frost", "unstable_arcana", "weight", "Leap", "Dash", "Platform"};
    String[] movement_loot = new string[] {"Leap", "Dash", "Platform"};
    public override void _Ready()
    {
        enemyNearby = this.FindChild<enemyNearby>();
        Connect("area_entered", this, nameof(_AreaEntered));
        red = GetNode<Particles2D>("red");
        green = GetNode<Particles2D>("green");
        red.Emitting = true;
        _openSound = GetNode<AudioStreamPlayer>("OpenChest");
        _animation = this.FindChild<AnimationPlayer>();
    }

    
    
    private void _AreaEntered(Area2D a)
    {
        
        if (a is StatusArea status && enemyNearby.ChestEnemies.Count < 1 && status.Team == Team.PLAYER && !_opened)
        {
            if (!GameState.Instance.FirstMovement)
            {
                GameState.Instance.FirstMovement = true;
                lootItem = SpellTokens.MakeToken(movement_loot[GD.Randi() % movement_loot.Length]);
            }
            else
            {
                lootItem = SpellTokens.MakeToken(loot[GD.Randi() % loot.Length]);
            }
            Inventory.Instance.AddSpell(lootItem);
            _openSound.Play();
            _animation.Play("Open");
            SetDeferred("monitoring", false);
            SetPhysicsProcess(false);
            red.Emitting = false;
            green.Emitting = false;
            _opened = true;
        }
    }

    public override void _PhysicsProcess(float delta)
    {
        if (enemyNearby.hasEnemy())
        {
            red.Emitting = true;
            green.Emitting = false;
        }else
        {
            red.Emitting = false;
            green.Emitting = true;
        }
}
}

