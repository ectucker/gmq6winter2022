using Godot;
using System;

public class Door : Area2D
{
    // Declare member variables here. Examples:
    // private int a = 2;
    // private string b = "text";

    // Called when the node enters the scene tree for the first time.
    public override void _Ready()
    {
        Connect("area_entered", this, nameof(_AreaEntered));
        CompassNeedle.Instance.Door = this;
    }

    private void _AreaEntered(Area2D a)
    {
        if (a is StatusArea status)
        {
            if (status.Team == Team.PLAYER)
            {
                GameState.Instance.NextLevel(this);
                status.Invincible = true;
            }
        }
    }

//  // Called every frame. 'delta' is the elapsed time since the previous frame.
//  public override void _Process(float delta)
//  {
//      
//  }
}
