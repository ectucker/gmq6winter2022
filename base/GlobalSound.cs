using Godot;
using System;

public class GlobalSound : Node
{
    public static GlobalSound Instance { get; private set; }
    public AudioStreamPlayer damage1;
    public AudioStreamPlayer damage2;
    public AudioStreamPlayer damage3;
    public AudioStreamPlayer freeze;
    public AudioStreamPlayer hooked;
    public AudioStreamPlayer punched;
    public AudioStreamPlayer targeted;
    public AudioStreamPlayer smash;
    public AudioStreamPlayer base_sound;
    public AudioStreamPlayer quasar_sound;
    public AudioStreamPlayer slug_sound;
    public AudioStreamPlayer unstable;
    public AudioStreamPlayer dash;
    public AudioStreamPlayer leap;
    public override void _Ready()
    {
        Instance = this;
        damage1 = GetNode<AudioStreamPlayer>("damage1");
        damage2 = GetNode<AudioStreamPlayer>("damage2");
        damage3 = GetNode<AudioStreamPlayer>("damage3");
        freeze = GetNode<AudioStreamPlayer>("freeze");
        hooked = GetNode<AudioStreamPlayer>("hooked");
        punched = GetNode<AudioStreamPlayer>("punched");
        targeted = GetNode<AudioStreamPlayer>("targeted");
        smash = GetNode<AudioStreamPlayer>("smash");
        base_sound = GetNode<AudioStreamPlayer>("base_sound");
        quasar_sound = GetNode<AudioStreamPlayer>("quasar_sound");
        slug_sound = GetNode<AudioStreamPlayer>("slug_sound");
        unstable = GetNode<AudioStreamPlayer>("unstable");
        dash = GetNode<AudioStreamPlayer>("dash");
        leap = GetNode<AudioStreamPlayer>("leap");
    }
}
