using Godot;
using System;
using System.Threading.Tasks;

public class bat : EnemyBase
{
    /// <summary>
    /// The maximum speed of the enemy, in pixels per second.
    /// </summary>
    public override Vector2 MaxSpeed => _maxSpeed;
    private Vector2 _maxSpeed = new Vector2(200.0f, 200.0f);
    private Vector2 MaxSpeedNorm = new Vector2(200.0f, 200.0f);
    private Vector2 MaxSpeedSwoop = new Vector2(600.0f, 600.0f);
   
    /// <summary>
    /// Ground acceleration of the enemy, as a factor of MaxSpeed.
    /// </summary>
    public override float GroundAcceleration => 4.0f;

    /// <summary>
    /// Air acceleration of the enemy, as a factor of MaxSpeed.
    /// </summary>
    public override float AirAcceleration => 4.0f;

    /// <summary>
    /// Ground friction of the enemy, as a factor of their current speed.
    /// </summary>
    public override float GroundFriction => 5.0f;

    /// <summary>
    /// Ground friction of the enemy, as a factor of MaxSpeed.
    /// </summary>
    public override float AirFriction => 5.0f;

    /// <summary>
    /// Ground friction of the enemy, in pixels per second * kg.
    /// </summary>
    public override float JumpImpulse => 1200.0f;

    /// <summary>
    /// The time the enemy will wait between attacking the player and returning to an idle/pursue state.
    /// </summary>
    protected override float HitCooldown => 1.0f;

    /// <summary>
    /// The minimum distance the enemy wants to be from the player when pursing.
    /// </summary>
    protected override float PlayerTargetRangeMin => 20.0f;

    /// <summary>
    /// The maximum distance the enemy wants to be from the player when pursuing.
    /// </summary>
    protected override float PlayerTargetRangeMax => 32.0f;

    /// <summary>
    /// The mass of the enemy, in kg.
    /// This decreases the strength of knockback effects.
    /// </summary>
    protected virtual float Mass => 1.0f;

    /// <summary>
    /// Called after the player is hit by this enemy's damage box (but not projectiles)
    /// </summary>
    /// <param name="status">The player status area</param>
    /// <param name="amount">The amount of damage dealt</param>
    private AudioStreamPlayer swoop;

    public override float Gravity
    {
        get => 0.0f; 
        set {}
    }

    private float _idleTime = 0;

    public override void _Ready()
    {
        base._Ready();
        swoop = GetNode<AudioStreamPlayer>("bat_swoop");
    }


    protected override void PlayerHitEffect(StatusArea status, int amount)
    {

    }

    protected override void IdleState()
    {
        base.IdleState();

        _idleTime += GetProcessDeltaTime();
        AccelDir = new Vector2(Mathf.Sin(_idleTime), Mathf.Cos(_idleTime));
        _animation.PlayIfNot("flap");
    }

    protected override void PursueState()
    {
        base.PursueState();

        AccelDir = (PlayerFinder.GetPlayer().GlobalPosition - GlobalPosition).Normalized();
        _animation.PlayIfNot("flap");
    }

    protected override bool ShouldAttack()
    {
        var spaceState = GetWorld2d().DirectSpaceState;
        var result = spaceState.IntersectRay(GlobalPosition, PlayerFinder.GetPlayer().GlobalPosition, null, 0b00000000000000000001);
        return (result.Keys.Count == 0 || (GlobalPosition - (Vector2) result["position"]).Length() > 64.0);
    }

    protected override async Task Attack()
    {
        _maxSpeed = MaxSpeedSwoop;
        AccelDir = (PlayerFinder.GetPlayer().GlobalPosition - GlobalPosition).Normalized();
        swoop.Play();
        await Awaiters.Delay(0.8f);
        _maxSpeed = MaxSpeedNorm;
        SetStopAccel();
        await Awaiters.Delay(1.5f);
    }
}
