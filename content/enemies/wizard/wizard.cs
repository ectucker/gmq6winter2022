using Godot;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

public class wizard : EnemyBase
{
  /// <summary>
    /// The maximum speed of the enemy, in pixels per second.
    /// </summary>
    public override Vector2 MaxSpeed => new Vector2(310.0f, 0.0f);

    /// <summary>
    /// Ground acceleration of the enemy, as a factor of MaxSpeed.
    /// </summary>
    public override float GroundAcceleration => 4.0f;

    /// <summary>
    /// Air acceleration of the enemy, as a factor of MaxSpeed.
    /// </summary>
    public override float AirAcceleration => 4.0f;

    /// <summary>
    /// Ground friction of the enemy, as a factor of their current speed.
    /// </summary>
    public override float GroundFriction => 10.0f;

    /// <summary>
    /// Ground friction of the enemy, as a factor of MaxSpeed.
    /// </summary>
    public override float AirFriction => 2.0f;

    /// <summary>
    /// Ground friction of the enemy, in pixels per second * kg.
    /// </summary>
    public override float JumpImpulse => 1200.0f;

    /// <summary>
    /// The time the enemy will wait between attacking the player and returning to an idle/pursue state.
    /// </summary>
    protected override float HitCooldown => 3.0f;

    /// <summary>
    /// The minimum distance the enemy wants to be from the player when pursing.
    /// </summary>
    protected override float PlayerTargetRangeMin => 250.0f;

    /// <summary>
    /// The maximum distance the enemy wants to be from the player when pursuing.
    /// </summary>
    protected override float PlayerTargetRangeMax => 400.0f;

    /// <summary>
    /// The mass of the enemy, in kg.
    /// This decreases the strength of knockback effects.
    /// </summary>
    protected virtual float Mass => 1.0f;

    /// <summary>
    /// Called after the player is hit by this enemy's damage box (but not projectiles)
    /// </summary>
    /// <param name="status">The player status area</param>
    /// <param name="amount">The amount of damage dealt</param>

    private float timeSinceShot = 0;
    private Spell spell;
    private AudioStreamPlayer cast;
    public override void _Ready()
    {
        base._Ready();
        //list of spells
        //random one chosen set that as spell
        GD.Randomize();
        List<string>[] spell_choices = new []
        {
            new List<string>(new []{"slug", "slug", "TargetLock", "quasar"}),
            new List<string>(new []{"AggressiveLaunch", "AggressiveLaunch", "AggressiveLaunch", "AggressiveLaunch"}),
            new List<string>(new []{"quasar", "AggressiveLaunch", "AggressiveLaunch"}),
            new List<string>(new []{"hook_shot", "slug", "slug"})
        };
        spell = SpellParser.ParseSpell(spell_choices[GD.Randi() % spell_choices.Length]);
        cast = GetNode<AudioStreamPlayer>("wizard_cast_spell");
    }
    
    protected override void PlayerHitEffect(StatusArea status, int amount) { }

    protected override bool ShouldAttack()
    {
        var spaceState = GetWorld2d().DirectSpaceState;
        var result = spaceState.IntersectRay(GlobalPosition, PlayerFinder.GetPlayer().GlobalPosition, null, 0b00000000000000000001);
        return (result.Keys.Count == 0 || (GlobalPosition - (Vector2) result["position"]).Length() > 64.0);
    }

    protected override void IdleState()
    {
        base.IdleState();
        
    }

    protected override void PursueState()
    {
        base.PursueState();
        
    }

    protected override async Task Attack()
    {
        SetStopAccel();
        _animation.Stop(true);
        await Awaiters.PhysicsFrame();
        _animation.Play("raise_wand");
        await Awaiters.Signal(_animation, "animation_finished");
        spell.Cast(Team.ENEMY, GlobalPosition + Vector2.Up * 120.0f, PlayerFinder.GetPlayer().GlobalPosition + Vector2.Up * 64.0f);
        _animation.Play("cast_spell");
        await Awaiters.Signal(_animation, "animation_finished");
        await Awaiters.Delay(0.3f);
    }
}
    
    

