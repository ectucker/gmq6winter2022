using Godot;
using System;
using System.Threading.Tasks;

public class beetle : EnemyBase
{
    /// <summary>
    /// The maximum speed of the enemy, in pixels per second.
    /// </summary>
    private Vector2 _maxSpeed = new Vector2(310.0f, 0.0f);
    public override Vector2 MaxSpeed => new Vector2(310.0f, 0.0f);
    private Vector2 MaxSpeedNorm = new Vector2(310.0f, 0.0f);
    private Vector2 MaxSpeedCharge = new Vector2(700.0f, 0.0f);
    /// <summary>
    /// Ground acceleration of the enemy, as a factor of MaxSpeed.
    /// </summary>
    public override float GroundAcceleration => 4.0f;
    /// <summary>
    /// Air acceleration of the enemy, as a factor of MaxSpeed.
    /// </summary>
    public override float AirAcceleration => 4.0f;
    /// <summary>
    /// Ground friction of the enemy, as a factor of their current speed.
    /// </summary>
    public override float GroundFriction => 10.0f;
    /// <summary>
    /// Ground friction of the enemy, as a factor of MaxSpeed.
    /// </summary>
    public override float AirFriction => 2.0f;
    /// <summary>
    /// Ground friction of the enemy, in pixels per second * kg.
    /// </summary>
    public override float JumpImpulse => 1200.0f;
    /// <summary>
    /// The time the enemy will wait between attacking the player and returning to an idle/pursue state.
    /// </summary>
    protected override float HitCooldown => 1.0f;
    /// <summary>
    /// The minimum distance the enemy wants to be from the player when pursing.
    /// </summary>
    protected override float PlayerTargetRangeMin => 32.0f;
    /// <summary>
    /// The maximum distance the enemy wants to be from the player when pursuing.
    /// </summary>
    protected override float PlayerTargetRangeMax => 64.0f;
    /// <summary>
    /// The mass of the enemy, in kg.
    /// This decreases the strength of knockback effects.
    /// </summary>
    protected virtual float Mass => 1.0f;

    private AudioStreamPlayer charge;

    public override void _Ready()
    {
        base._Ready();
        charge = GetNode<AudioStreamPlayer>("beetle_charge");

    }


    /// <summary>
    /// Called after the player is hit by this enemy's damage box (but not projectiles)
    /// </summary>
    /// <param name="status">The player status area</param>
    /// <param name="amount">The amount of damage dealt</param>
    protected override void PlayerHitEffect(StatusArea status, int amount)
    {
        if (ToLocal(status.GlobalPosition).x > 0)
        {
            ApplyImpulse(Vector2.Left * 1000.0f);
           
        }
        else if (ToLocal(status.GlobalPosition).x < 0)
        {
            ApplyImpulse(Vector2.Right * 1000.0f);
            
        }
        
        if(IsOnFloor())
         ApplyImpulse(Vector2.Up * 750.0f);

            
    }

    /// <summary>
    /// Determines if this enemy should attack.
    /// </summary>
    /// <returns>True if this enemy should attack</returns>
    protected override bool ShouldAttack()
    {
        return Mathf.Abs((PlayerFinder.GetPlayer().GlobalPosition - GlobalPosition).x) < 248.0f;
    }

    protected override void IdleState()
    {
        base.IdleState();
        
        _animation.PlayIfNot("walk");
    }

    protected override void PursueState()
    {
        base.PursueState();
        
        _animation.PlayIfNot("walk");
    }

    protected override async Task Attack()
    {
        float dir = Mathf.Sign((PlayerFinder.GetPlayer().GlobalPosition - GlobalPosition).x);
        SetStopAccel();
        _animation.Stop(true);
        await Awaiters.PhysicsFrame();
        charge.Play();
        _animation.Play("horn_down");
        await Awaiters.Signal(_animation, "animation_finished");
        _maxSpeed = MaxSpeedCharge;
        AccelDirX = dir * 8.0f;
        
        _animation.Play("charge");
        await Awaiters.Delay(1.0f);
        SetStopAccel();
        _maxSpeed = MaxSpeedNorm;
        await Awaiters.Delay(0.3f);
    }
}
