using Godot;
using System.Collections.Generic;
using System.Threading.Tasks;

public class BasicShootingEnemy : EnemyBase
{
    private float timeSinceShot = 0;
    private Spell spell;

    public override void _Ready()
    {
        base._Ready();
        //list of spells
        //random one chosen set that as spell
        spell = SpellParser.ParseSpell(new List<string>(new []{"Fireball"}));
    }
    
    protected override void PlayerHitEffect(StatusArea status, int amount) { }

    protected override bool ShouldAttack()
    {
        var spaceState = GetWorld2d().DirectSpaceState;
        var result = spaceState.IntersectRay(GlobalPosition, PlayerFinder.GetPlayer().GlobalPosition);
        return (result.Keys.Count == 0 || (GlobalPosition - (Vector2) result["position"]).Length() > 128.0);
    }

    protected override async Task Attack()
    {
        spell.Cast(Team.ENEMY, GlobalPosition, PlayerFinder.GetPlayer().GlobalPosition);
        await Awaiters.Delay(0.3f);
    }
}
