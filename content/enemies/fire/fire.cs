using Godot;
using System;
using System.Threading.Tasks;

public class fire : EnemyBase
{
    /// <summary>
    /// The maximum speed of the enemy, in pixels per second.
    /// </summary>
    public override Vector2 MaxSpeed => new Vector2(240.0f, 0.0f);

    /// <summary>
    /// Ground acceleration of the enemy, as a factor of MaxSpeed.
    /// </summary>
    public override float GroundAcceleration => 2.0f;

    /// <summary>
    /// Air acceleration of the enemy, as a factor of MaxSpeed.
    /// </summary>
    public override float AirAcceleration => 6.0f;

    /// <summary>
    /// Ground friction of the enemy, as a factor of their current speed.
    /// </summary>
    public override float GroundFriction => 10.0f;

    /// <summary>
    /// Ground friction of the enemy, as a factor of MaxSpeed.
    /// </summary>
    public override float AirFriction => 2.0f;

    /// <summary>
    /// Ground friction of the enemy, in pixels per second * kg.
    /// </summary>
    public override float JumpImpulse => 1500.0f;

    /// <summary>
    /// The time the enemy will wait between attacking the player and returning to an idle/pursue state.
    /// </summary>
    protected override float HitCooldown => 1.0f;

    /// <summary>
    /// The minimum distance the enemy wants to be from the player when pursing.
    /// </summary>
    protected override float PlayerTargetRangeMin => 32.0f;

    /// <summary>
    /// The maximum distance the enemy wants to be from the player when pursuing.
    /// </summary>
    protected override float PlayerTargetRangeMax => 64.0f;

    /// <summary>
    /// The mass of the enemy, in kg.
    /// This decreases the strength of knockback effects.
    /// </summary>
    protected virtual float Mass => 1.0f;

    private AudioStreamPlayer jump;
    public override void _Ready()
    {
        base._Ready();
        jump = GetNode<AudioStreamPlayer>("fire_wisp_bounce");
    }
    
    
    
    /// <summary>
    /// Called after the player is hit by this enemy's damage box (but not projectiles)
    /// </summary>
    /// <param name="status">The player status area</param>
    /// <param name="amount">The amount of damage dealt</param>

    protected override void PlayerHitEffect(StatusArea status, int amount)
    {

    }

    protected override void IdleState()
    {
        SetStopAccel();
        _animation.PlayIfNot("idle");
    }

    protected override void PursueState()
    {
        base.PursueState();
        
        _animation.PlayIfNot("jump");
        if (IsOnFloor() && _animation.CurrentAnimationPosition > 0.6f && _animation.CurrentAnimationPosition < 0.7f)
            ApplyImpulse(Vector2.Up * 500.0f);
    }

    protected override bool ShouldAttack()
    {
        return false;
    }

    protected override async Task Attack()
    {

    }
}
