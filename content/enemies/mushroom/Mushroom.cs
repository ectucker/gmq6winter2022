using Godot;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

public class Mushroom : EnemyBase
{
  /// <summary>
    /// The maximum speed of the enemy, in pixels per second.
    /// </summary>
    public override Vector2 MaxSpeed => new Vector2(180.0f, 0.0f);

    /// <summary>
    /// Ground acceleration of the enemy, as a factor of MaxSpeed.
    /// </summary>
    public override float GroundAcceleration => 4.0f;

    /// <summary>
    /// Air acceleration of the enemy, as a factor of MaxSpeed.
    /// </summary>
    public override float AirAcceleration => 4.0f;

    /// <summary>
    /// Ground friction of the enemy, as a factor of their current speed.
    /// </summary>
    public override float GroundFriction => 10.0f;

    /// <summary>
    /// Ground friction of the enemy, as a factor of MaxSpeed.
    /// </summary>
    public override float AirFriction => 2.0f;

    /// <summary>
    /// Ground friction of the enemy, in pixels per second * kg.
    /// </summary>
    public override float JumpImpulse => 500.0f;

    /// <summary>
    /// The time the enemy will wait between attacking the player and returning to an idle/pursue state.
    /// </summary>
    protected override float HitCooldown => 1.5f;

    /// <summary>
    /// The minimum distance the enemy wants to be from the player when pursing.
    /// </summary>
    protected override float PlayerTargetRangeMin => 20.0f;

    /// <summary>
    /// The maximum distance the enemy wants to be from the player when pursuing.
    /// </summary>
    protected override float PlayerTargetRangeMax => 32.0f;

    /// <summary>
    /// The mass of the enemy, in kg.
    /// This decreases the strength of knockback effects.
    /// </summary>
    protected virtual float Mass => 1.0f;


    private Spell spell;
    private AudioStreamPlayer charge;
    private AudioStreamPlayer shoot;
    public override void _Ready()
    {
        base._Ready();
        charge = GetNode<AudioStreamPlayer>("gather_spores");
        shoot = GetNode<AudioStreamPlayer>("emit_spores");
        spell = SpellParser.ParseSpell(new List<string>(new []{"MushroomMod"}));
    }


    /// <summary>
    /// Called after the player is hit by this enemy's damage box (but not projectiles)
    /// </summary>
    /// <param name="status">The player status area</param>
    /// <param name="amount">The amount of damage dealt</param>

    protected override void PlayerHitEffect(StatusArea status, int amount)
    {

    }

    protected override void IdleState()
    {
        base.IdleState();
        
        _animation.PlayIfNot("walk");
    }

    protected override void PursueState()
    {
        base.PursueState();
        
        _animation.PlayIfNot("walk");
    }

    protected override bool ShouldAttack()
    {
        return Mathf.Abs(PlayerFinder.GetPlayer().GlobalPosition.x - GlobalPosition.x) <= PlayerTargetRangeMax;
    }

    protected override async Task Attack()
    {
        SetStopAccel();
        _animation.Stop();
        await Awaiters.PhysicsFrame();
        charge.Play();
        // TODO animation
        _animation.Play("spores");
        await Awaiters.Signal(_animation, "animation_finished");
        shoot.Play();
        spell.Cast(Team.ENEMY, new Vector2(GlobalPosition.x, GlobalPosition.y - 70.0f), new Vector2(GlobalPosition.x, GlobalPosition.y - 200.0f));
        await Awaiters.Delay(0.5f);
    }
}
