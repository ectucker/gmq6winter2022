﻿using Godot;
using System;

public class SkeletonMod : ModifierComponent
{
    public override string ModiferPath => "res://content/spells/skeleton_mod/skeleton_mod.tscn";

    public override bool HidesMain => true;
}