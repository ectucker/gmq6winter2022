using Godot;
using System;

public class levitate : ModifierComponent
{
    public override string ModiferPath => "res://content/spells/levitate/levitate.tscn";
    public override void ProjectileHit(Projectile proj, StatusArea status)
    {
        status.addStatusEffects(StatusEffect.statisType.levitate,3,0);
    }
}
