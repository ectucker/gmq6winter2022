using Godot;
using System;

public class lizard_mod : ModifierComponent
{
    public override string ModiferPath => "res://content/spells/lizard_mod/lizard_mod.tscn";
    
    public override bool HidesMain => true;
}
