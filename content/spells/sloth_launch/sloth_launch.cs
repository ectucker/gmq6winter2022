using Godot;
using System;

public class sloth_launch : ModifierComponent
{
    
    public override string ModiferPath => "res://content/spells/sloth_launch/sloth_launch.tscn";

    public override void ProjectileSpawned(Projectile proj)
    {
        proj.Speed *= 0.65f;
        
    }
}
