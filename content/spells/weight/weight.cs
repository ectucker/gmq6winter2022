using Godot;
using System;

public class weight : ModifierComponent
{
    public override string ModiferPath => "res://content/spells/weight/weight.tscn";
    private GlobalSound sound = GlobalSound.Instance;
    public override void ProjectileHit(Projectile proj, StatusArea status)
    {
        sound.smash.Playing = true;
        status.addStatusEffects(StatusEffect.statisType.weight, 0.5f, 1);
        if(status.Entity.IsOnFloor())
            status.Damage(0.25f * proj.Damage, proj.GlobalPosition);
        else
        {
            status.Damage(0.5f * proj.Damage, proj.GlobalPosition);
        }
    }
}
