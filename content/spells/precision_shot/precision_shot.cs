using Godot;
using System;

public class precision_shot : ModifierComponent
{
    public override string ModiferPath => "res://content/spells/precision_shot/precision_shot.tscn";

    public override void ProjectileHit(Projectile proj, StatusArea status)
    {
        if (status.Entity.Velocity.Length() < 0.1f)
        {
            status.Damage(proj.Damage * 0.5f, proj.GlobalPosition);
        }
    }
}
