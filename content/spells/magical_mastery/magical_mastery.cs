using Godot;
using System;

public class magical_mastery : ModifierComponent
{
    public override string ModiferPath => "res://content/spells/magical_mastery/magical_mastery.tscn";

    public override void ProjectileSpawned(Projectile proj)
    {
        proj.Damage += 2.5f;
    }
}

