using Godot;
using System;

public class slug : ModifierComponent
{
    public override string ModiferPath => "res://content/spells/slug/slug.tscn";
    private float timeToZero = 0.5f;
    private GlobalSound sound = GlobalSound.Instance;

    public override void ProjectileSpawned(Projectile proj)
    {
        base.ProjectileSpawned(proj);
        sound.slug_sound.Playing = true;
    }

    public override void ProjectileHit(Projectile proj, StatusArea status)
    {
        if (timeToZero - proj.Time > 0)
        {
            status.Damage(proj.Damage * 0.5f * (timeToZero - proj.Time), proj.GlobalPosition);
        }
    }
}
