﻿public class TargetLock : ModifierComponent
{
    public override string ModiferPath => "res://content/spells/target_lock/target_lock.tscn";
    private GlobalSound sound = GlobalSound.Instance;
    public override void ProjectileHit(Projectile proj, StatusArea status)
    {
        sound.targeted.Playing = true;
        status.addStatusEffects(StatusEffect.statisType.targeted, 5.0f, 1.0f);
    }
}
