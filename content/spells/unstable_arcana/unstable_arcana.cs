using Godot;
using System;

public class unstable_arcana : ModifierComponent
{
    public override string ModiferPath => "res://content/spells/unstable_arcana/unstable_arcana.tscn";
    private GlobalSound sound = GlobalSound.Instance;

    public override void ProjectileSpawned(Projectile proj)
    {
        base.ProjectileSpawned(proj);
        sound.unstable.Playing = true;
    }


    public override void ProjectileHit(Projectile proj, StatusArea status)
    {
       
            status.Damage(proj.Damage * 0.5f * proj.Time, proj.GlobalPosition);
        
    }
}
