﻿using Godot;
using System;

public class MushroomMod : ModifierComponent
{
    public override string ModiferPath => "res://content/spells/mushroom_mod/mushroom_mod.tscn";

    public override bool HidesMain => true;
}