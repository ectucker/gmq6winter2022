﻿public class AggressiveLaunch : ModifierComponent
{
    public override string ModiferPath => "res://content/spells/aggressive_launch/aggressive_launch.tscn";

    public override void ProjectileSpawned(Projectile proj)
    {
        base.ProjectileSpawned(proj);

        proj.Speed += 0.35f * proj.BaseSpeed;
    }
}