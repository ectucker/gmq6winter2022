using Godot;
using System;

public class quasar : ModifierComponent
{
    public override string ModiferPath => "res://content/spells/quasar/quasar.tscn";
    private Random r = new Random();
    private GlobalSound sound = GlobalSound.Instance;
    public override void ProjectileSpawned(Projectile proj)
    {
        sound.quasar_sound.Playing = true;
        float rotation = (float) (r.Next(0, 60) - 30);
        
       proj.Direction =  proj.Direction.Rotated(Mathf.Deg2Rad(rotation));
    }

    public override void ProjectileHit(Projectile proj, StatusArea status)
    {
        
        status.Damage((proj.Damage * 0.75f), proj.GlobalPosition);
    }
}
