using Godot;
using System;

public class hook_shot : ModifierComponent
{
   public override string ModiferPath => "res://content/spells/hook_shot/hook_shot.tscn";
   private GlobalSound sound = GlobalSound.Instance;
   public override void ProjectileHit(Projectile proj, StatusArea status)
   {
      sound.hooked.Playing = true;
      status.Entity.ApplyImpulse(-proj.Direction * proj.Speed * 1.0f);
   }
}
