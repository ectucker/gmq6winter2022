using Godot;
using System;

public class touch_of_frost : ModifierComponent
{
    public override string ModiferPath => "res://content/spells/touch_of_frost/touch_of_frost.tscn";
    private GlobalSound sound = GlobalSound.Instance;
    public override void ProjectileHit(Projectile proj, StatusArea status)
    {
        status.addStatusEffects(StatusEffect.statisType.freeze, 3, 0);
        sound.freeze.Playing = true;
    }
}
