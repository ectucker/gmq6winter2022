using Godot;
using System;

public class packed_punch : ModifierComponent
{
    public override string ModiferPath => "res://content/spells/packed_punch/packed_punch.tscn";
    private GlobalSound sound = GlobalSound.Instance;
    public override void ProjectileHit(Projectile proj, StatusArea status)
    {
        sound.punched.Playing = true;
        status.Entity.ApplyImpulse(new Vector2(proj.Direction.x * proj.Speed * 1.0f, (proj.Direction.y-0.25f) * proj.Speed * 1.0f));
        status.Damage(proj.Damage * 0.25f, proj.GlobalPosition);
    }
}
