﻿public static class SpellTokens
{
    public static SpellToken MakeToken(string name)
    {
        switch (name)
        {
            case "AggressiveLaunch":
                return new SpellToken("Aggressive Launch", 
                    "res://content/spells/aggressive_launch/armored-boomerang.png", 
                    "Your magic has increased velocity", 
                    "The blistered cracked stone feels surprisingly warm to the touch",
                    typeof(AggressiveLaunch));
            case "hook_shot":
                return new SpellToken("Hook Shot",
                 "res://content/spells/hook_shot/harpoon-chain.png",
                "Your magic drags the enemy closer",
                 "Some invisible force draws you towards the rune",
                 typeof(hook_shot));
            case "levitate":
                return new SpellToken("Levitate",
                    "res://content/spells/levitate/up-card.png",
                    "Your magic lifts the enemy into the air for a brief moment",
                    "The stone jostles to itself upon you discovering it actually weighs quite a lot",
                    typeof(levitate));
            case "magical_mastery":
                return new SpellToken("Magical Mastery",
                    "res://content/spells/magical_mastery/growth.png",
                    "Your magic has increased damage",
                    "A flow of what feels like life itself courses through you",
                    typeof(magical_mastery));
            case "packed_punch":
                return new SpellToken("Packed Punch",
                    "res://content/spells/packed_punch/brass-knuckles.png",
                    "Your magic knocks the enemy back a considerable distance",
                    "The rune hums to life in your hand - vibrating aggressively",
                    typeof(packed_punch));
            case "precision_shot":
                return new SpellToken("Precision Shot",
                    "res://content/spells/precision_shot/convergence-target.png",
                    "",
                    "",
                    typeof(precision_shot));
            case "quasar":
                return new SpellToken("Quasar",
                    "res://content/spells/quasar/chaingun.png",
                    "Your magic wields immense damage but in an uncontrollable projectile ",
                    "The force given off by the innocent looking rock is enough to inspire armies",
                    typeof(quasar));
            case "sloth_launch":
                return new SpellToken("Sloth Launch",
                    "res://content/spells/sloth_launch/sloth.png",
                    "Your magic has decreased velocity",
                    "The stone clutched in your hand sluggishly buzzes to life with immeasurable happiness",
                    typeof(sloth_launch));
            case "slug":
                return new SpellToken("Slug",
                    "res://content/spells/slug/backward-time.png",
                    "Your magic has Increased damage but fizzles out with time",
                    "A gray ichor oozes forth from the inside of the sleek rock",
                    typeof(slug));
            case "TargetLock":
                return new SpellToken("Target Lock",
                    "res://content/spells/target_lock/crosshair.png",
                    "Future magic will home in towards the enemy until damaged",
                    "Calm and collected, the rune in your hand methodically thumps along to your pulsing veins",
                    typeof(TargetLock));
            case "touch_of_frost":
                return new SpellToken("Touch of Frost",
                    "res://content/spells/touch_of_frost/frozen-arrow.png",
                    "Freezes enemy until damaged",
                    "A chilly aura emanates from the rustic stone in your hand",
                    typeof(touch_of_frost));
            case "unstable_arcana":
                return new SpellToken("Unstable Arcana",
                    "res://content/spells/unstable_arcana/triple-beak.png",
                    "Your magic has increasing damage with each passing second",
                    "The magic aura surrounding the gemstone ruptures time itself",
                    typeof(unstable_arcana));
            case "weight":
                return new SpellToken("Weight",
                    "res://content/spells/weight/despair.png",
                    "Your magic smashes the enemy into the earth",
                    "The whirs and ticks in this gemstones stomach suggest an eagerness to show off",
                    typeof(weight));
            case "Leap":
                return new SpellToken("Leap",
                    "res://content/movements/leap/arrow-dunk.png",
                    "An additional jump!",
                    "This rock is quite tricky to hold",
                    typeof(Leap));
            case "Dash":
                return new SpellToken("Phase",
                    "res://content/movements/dash/sprint.png",
                    "Dashes towards the cursor",
                    "The gemstone seems to periodically phase in and out of existence ",
                    typeof(Dash));
            case "Platform":
                return new SpellToken("Summon Earth",
                    "res://content/movements/platform/flat-platform.png",
                    "Creates a semi-sturdy platform beneath your feet",
                    "The tiny pebble clutched in your hand is eager to become something much greater",
                    typeof(Platform));
            case "lizard_mod":
                return new SpellToken("lizard_mod",
                    "res://content/movements/platform/flat-platform.png",
                    "",
                    "",
                    typeof(lizard_mod));
            case "SkeletonMod":
                return new SpellToken("skeleton_mod",
                    "res://content/movements/platform/flat-platform.png",
                    "",
                    "",
                    typeof(SkeletonMod));
            case "MushroomMod":
                return new SpellToken("mushroom_mod",
                    "res://content/movements/platform/flat-platform.png",
                    "",
                    "",
                    typeof(MushroomMod));
            default:
                return new SpellToken("Null", 
                    "res://util/empty.png", 
                    "", 
                    "",
                    typeof(SummonProjectile));
        }
    }
}
