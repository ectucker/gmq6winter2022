﻿using System.Threading.Tasks;

public class Leap : MovementComponent
{
    private const float JUMP_IMPULSE = 1200.0f;

    public override string ParticlePath => "res://content/movements/leap/leap.tscn";
    
    public override async Task Apply(Player player)
    {
        player.OverrideAnim("jump");
        GlobalSound.Instance.leap.Play();
        if (player.VelocityY > 0)
            player.VelocityY = 0;
        if (player.VelocityY > -JUMP_IMPULSE)
            player.VelocityY = -JUMP_IMPULSE;
    }
}