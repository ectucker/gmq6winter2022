﻿using System.Threading.Tasks;
using Godot;

public class Dash : MovementComponent
{
    public override string ParticlePath => "res://content/movements/dash/dash.tscn";
    
    public override async Task Apply(Player player)
    {
        Vector2 dir = (InputUtil.GetWorldMousePosition() - player.GlobalPosition).Normalized();
        player.ApplyImpulse(dir * 1400.0f);
        _effect.GlobalRotation = Mathf.Atan2(dir.y, dir.x);
        GlobalSound.Instance.dash.Play();
    }
}