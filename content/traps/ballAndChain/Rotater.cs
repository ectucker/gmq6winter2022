using Godot;
using System;

public class Rotater : Node2D
{
    [Export()] private float speed = 5;
    [Export()] private int length = 5;

    // Declare member variables here. Examples:
    // private int a = 2;
    // private string b = "text";

    // Called when the node enters the scene tree for the first time.
    public override void _Ready()
    {
        Area2D ball = (Area2D)GetNode("Ball");
        ball.GlobalPosition = new Vector2(GlobalPosition.x + (length * 32), GlobalPosition.y);
        for (int i = 0; i < length; i++)
        {
            Sprite s = new Sprite();
            AddChild(s);
            Texture img = (Texture) GD.Load("res://content/traps/ballAndChain/chain.png");
            s.Texture = img;
            s.RotationDegrees = 90;
            s.GlobalPosition = new Vector2(GlobalPosition.x + (32 * i), GlobalPosition.y);
            
        }
    }

//  // Called every frame. 'delta' is the elapsed time since the previous frame.
 public override void _Process(float delta)
  {
      this.RotationDegrees += speed;
  }
}
