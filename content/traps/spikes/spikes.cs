using Godot;
using System;

public class spikes : Area2D
{
    [Export()] private int damage = 10;
    // Declare member variables here. Examples:
    // private int a = 2;
    // private string b = "text";

    private void _AreaEntered(Area2D a)
    {
        
        if (a is StatusArea status)
        {
            status.Damage(damage, GlobalPosition);
        }
        
        
    }
    
    // Called when the node enters the scene tree for the first time.
    public override void _Ready()
    {
        Connect("area_entered", this, nameof(_AreaEntered));
    }




    //  // Called every frame. 'delta' is the elapsed time since the previous frame.
//  public override void _Process(float delta)
//  {
//      
//  }
}
