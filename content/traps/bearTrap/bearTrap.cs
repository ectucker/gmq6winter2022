using Godot;
using System;
using System.Threading.Tasks;

public class bearTrap : Area2D
{
    // Declare member variables here. Examples:
    // private int a = 2;
    // private string b = "text";
    private Node s;
    private AnimatedSprite _sprite;
    private void _AreaEntered(Area2D a)
    {
        GD.Print("bear trap entered");
        //add delay

        Trap(a);
    }

    private async Task Trap(Area2D a)
    {
        await Awaiters.Delay(1.0f);
        
        GD.Print("delay done");
        _sprite.Frame = 1;
        //add delay
        if (this.OverlapsArea(a))
        {
            if (a is StatusArea status)
            {
                status.Damage(20, GlobalPosition);
            }
        }

        await Awaiters.Delay(1.0f);
        _sprite.Frame = 0;
    }
    // Called when the node enters the scene tree for the first time.
    public override void _Ready()
    {
        s = GetNode("trapSprite"); 
        _sprite = (AnimatedSprite) s;
        Connect("area_entered", this, nameof(_AreaEntered));
    }

//  // Called every frame. 'delta' is the elapsed time since the previous frame.
//  public override void _Process(float delta)
//  {
//      
//  }
}
