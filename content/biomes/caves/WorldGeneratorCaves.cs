using Godot;
using System;

public class WorldGeneratorCaves : WorldGenerator
{
    protected override void SetupParams()
    {
        base.SetupParams();

        _params.MidIds = new[] {12, 13};
        
        _params.Enemies = new []
        {
            new SpawnChance(0.9f, "res://content/enemies/skeleton/skeleton.tscn"),
            new SpawnChance(0.7f, "res://content/enemies/wizard/wizard.tscn"),
            new SpawnChance(0.5f, "res://content/enemies/beetle/beetle.tscn"),
            new SpawnChance(0.7f, "res://content/enemies/bat/bat.tscn")
        };

        _params.LadderTexturePath = "res://base/world/Ladder/Ladder.png";
    }
}
