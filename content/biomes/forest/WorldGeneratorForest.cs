using Godot;
using System;

public class WorldGeneratorForest : WorldGenerator
{
    protected override void SetupParams()
    {
        base.SetupParams();

        _params.MidIds = new[] {12, 13};
        
        _params.Enemies = new []
        {
            new SpawnChance(0.7f, "res://content/enemies/wizard/wizard.tscn"),
            new SpawnChance(0.5f, "res://content/enemies/beetle/beetle.tscn"),
            new SpawnChance(1.0f, "res://content/enemies/mushroom/mushroom.tscn")
        };

        _params.LadderTexturePath = "res://base/world/Ladder/Vines.png";
    }
}